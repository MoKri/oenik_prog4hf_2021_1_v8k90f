GameDataBaseFF

Function list:
1 - Lists the name of everything from the database.
2 - Lists the name of every game.
3 - Lists the name of every company.
4 - Lists the name of every platform.
5 - Games -- Lists every data of all the games/ Lists the datas of a single game/ Insert a game/ Change a game's price/ Remove a game
6 - Companies -- Lists every data of all the companies/ Lists the datas of a single company/ Insert a company/ Change a company's boss/ Remove a company
7 - Platforms -- Lists every data of all the platforms/ Lists the datas of a single platform/ Insert a platform/ Change a platform's storage/ Remove a platform
8 - Shows the average price of the games.
9 - Lists companies with their games. (Has an async version aswell)
10 - Lists the companies and shows how much their games are worth collectively. (Has an async version aswell)
11 - Lists those companies that have released platforms, and shows how many platforms they released. (Has an async version aswell)
12 - Shows which companie's platforms average price is the cheapest. (Has an async version aswell)
13 - Lists all the platforms that doesn't have an hdmi port. (Has an async version aswell)
14 - Quits the application.