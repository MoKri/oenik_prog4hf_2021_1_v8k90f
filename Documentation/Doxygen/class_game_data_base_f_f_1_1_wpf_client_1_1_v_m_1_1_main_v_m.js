var class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m =
[
    [ "MainVM", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m.html#aff850ac3f61719e44cb91dc38a06727b", null ],
    [ "MainVM", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m.html#a83fd736ee922875d25dbcd7ef05963c8", null ],
    [ "AddCmd", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m.html#ad47aa7c02072ce48a9c0469c842762ac", null ],
    [ "AllGames", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m.html#a1137943644d524cb206946ca827dd2ca", null ],
    [ "DelCmd", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m.html#a437f19e6a3c23a96ef84542ac0e8bb0b", null ],
    [ "EditorFunc", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m.html#ac90e04bcdf311a26e7682ee730e31c07", null ],
    [ "LoadCmd", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m.html#a8c51085502397405efc9e6ee68738275", null ],
    [ "ModCmd", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m.html#ac71dfd9b53672d8a7c70a252007e0edd", null ],
    [ "SelectedGame", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m.html#acbe23f1a5f2e49f6b26e5a5365c348fd", null ]
];