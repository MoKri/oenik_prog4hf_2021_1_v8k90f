var hierarchy =
[
    [ "GameDataBaseFF.Web.Controllers.ApiResult", "class_game_data_base_f_f_1_1_web_1_1_controllers_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "GameDataBaseFF.WpfClient.App", "class_game_data_base_f_f_1_1_wpf_client_1_1_app.html", null ],
      [ "GameWPF.App", "class_game_w_p_f_1_1_app.html", null ]
    ] ],
    [ "GameDataBaseFF.Logic.AveragePlatCostByComp", "class_game_data_base_f_f_1_1_logic_1_1_average_plat_cost_by_comp.html", null ],
    [ "GameDataBaseFF.Data.Company", "class_game_data_base_f_f_1_1_data_1_1_company.html", null ],
    [ "Controller", null, [
      [ "GameDataBaseFF.Web.Controllers.GamesApiController", "class_game_data_base_f_f_1_1_web_1_1_controllers_1_1_games_api_controller.html", null ],
      [ "GameDataBaseFF.Web.Controllers.GamesController", "class_game_data_base_f_f_1_1_web_1_1_controllers_1_1_games_controller.html", null ],
      [ "GameDataBaseFF.Web.Controllers.HomeController", "class_game_data_base_f_f_1_1_web_1_1_controllers_1_1_home_controller.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "GameDataBaseFF.Data.Models.GamesContext", "class_game_data_base_f_f_1_1_data_1_1_models_1_1_games_context.html", null ]
    ] ],
    [ "GameDataBaseFF.Logic.Tests.DevelopmentLogicTests", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_development_logic_tests.html", null ],
    [ "GameDataBaseFF.Logic.Tests.DeviceProduceLogicTests", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_device_produce_logic_tests.html", null ],
    [ "GameDataBaseFF.Web.Models.ErrorViewModel", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_error_view_model.html", null ],
    [ "GameDataBaseFF.Program.Factory", "class_game_data_base_f_f_1_1_program_1_1_factory.html", null ],
    [ "GameDataBaseFF.Web.Models.Game", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html", null ],
    [ "GameDataBaseFF.Logic.GameByCompany", "class_game_data_base_f_f_1_1_logic_1_1_game_by_company.html", null ],
    [ "GameDataBaseFF.Web.Models.GameListViewModel", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game_list_view_model.html", null ],
    [ "GameDataBaseFF.Logic.GamePriceByCompany", "class_game_data_base_f_f_1_1_logic_1_1_game_price_by_company.html", null ],
    [ "IComponentConnector", null, [
      [ "GameWPF.UI.GameEditorWindow", "class_game_w_p_f_1_1_u_i_1_1_game_editor_window.html", null ]
    ] ],
    [ "GameDataBaseFF.Logic.IDevelopmentLogic", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html", [
      [ "GameDataBaseFF.Logic.DevelopmentLogic", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html", null ]
    ] ],
    [ "GameDataBaseFF.Logic.IDeviceProduceLogic", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html", [
      [ "GameDataBaseFF.Logic.DeviceProduceLogic", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html", null ]
    ] ],
    [ "IDisposable", null, [
      [ "GameDataBaseFF.WpfClient.BL.MainLogic", "class_game_data_base_f_f_1_1_wpf_client_1_1_b_l_1_1_main_logic.html", null ]
    ] ],
    [ "GameWPF.BL.IEditorService", "interface_game_w_p_f_1_1_b_l_1_1_i_editor_service.html", [
      [ "GameWPF.UI.EditorViaWindow", "class_game_w_p_f_1_1_u_i_1_1_editor_via_window.html", null ]
    ] ],
    [ "GameWPF.BL.IGameLogic", "interface_game_w_p_f_1_1_b_l_1_1_i_game_logic.html", [
      [ "GameWPF.BL.GameLogic", "class_game_w_p_f_1_1_b_l_1_1_game_logic.html", null ]
    ] ],
    [ "GameDataBaseFF.WpfClient.BL.IMainLogic", "interface_game_data_base_f_f_1_1_wpf_client_1_1_b_l_1_1_i_main_logic.html", [
      [ "GameDataBaseFF.WpfClient.BL.MainLogic", "class_game_data_base_f_f_1_1_wpf_client_1_1_b_l_1_1_main_logic.html", null ]
    ] ],
    [ "InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "GameDataBaseFF.Repository.IRepository< T >", "interface_game_data_base_f_f_1_1_repository_1_1_i_repository.html", [
      [ "GameDataBaseFF.Repository.ParentRepo< T >", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html", null ]
    ] ],
    [ "GameDataBaseFF.Repository.IRepository< Company >", "interface_game_data_base_f_f_1_1_repository_1_1_i_repository.html", [
      [ "GameDataBaseFF.Repository.ICompanyRepository", "interface_game_data_base_f_f_1_1_repository_1_1_i_company_repository.html", [
        [ "GameDataBaseFF.Repository.CompanyRepository", "class_game_data_base_f_f_1_1_repository_1_1_company_repository.html", null ]
      ] ]
    ] ],
    [ "GameDataBaseFF.Repository.IRepository< Game >", "interface_game_data_base_f_f_1_1_repository_1_1_i_repository.html", [
      [ "GameDataBaseFF.Repository.IGameRepository", "interface_game_data_base_f_f_1_1_repository_1_1_i_game_repository.html", [
        [ "GameDataBaseFF.Repository.GameRepository", "class_game_data_base_f_f_1_1_repository_1_1_game_repository.html", null ]
      ] ]
    ] ],
    [ "GameDataBaseFF.Repository.IRepository< Platform >", "interface_game_data_base_f_f_1_1_repository_1_1_i_repository.html", [
      [ "GameDataBaseFF.Repository.IPlatformRepository", "interface_game_data_base_f_f_1_1_repository_1_1_i_platform_repository.html", [
        [ "GameDataBaseFF.Repository.PlatformRepository", "class_game_data_base_f_f_1_1_repository_1_1_platform_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "GameDataBaseFF.WpfClient.MyIoc", "class_game_data_base_f_f_1_1_wpf_client_1_1_my_ioc.html", null ],
      [ "GameWPF.MyIoc", "class_game_w_p_f_1_1_my_ioc.html", null ]
    ] ],
    [ "IValueConverter", null, [
      [ "GameDataBaseFF.WpfClient.UI.DateToStringConverter", "class_game_data_base_f_f_1_1_wpf_client_1_1_u_i_1_1_date_to_string_converter.html", null ],
      [ "GameWPF.UI.DateToBrushConverter", "class_game_w_p_f_1_1_u_i_1_1_date_to_brush_converter.html", null ],
      [ "GameWPF.UI.DateToStringConverter", "class_game_w_p_f_1_1_u_i_1_1_date_to_string_converter.html", null ],
      [ "GameWPF.UI.IntToCompanyConverter", "class_game_w_p_f_1_1_u_i_1_1_int_to_company_converter.html", null ],
      [ "GameWPF.UI.IntToPlatformNamesConverter", "class_game_w_p_f_1_1_u_i_1_1_int_to_platform_names_converter.html", null ],
      [ "GameWPF.UI.ListToCompanyNamesConverter", "class_game_w_p_f_1_1_u_i_1_1_list_to_company_names_converter.html", null ],
      [ "GameWPF.UI.ListToPlatformNamesConverter", "class_game_w_p_f_1_1_u_i_1_1_list_to_platform_names_converter.html", null ]
    ] ],
    [ "GameDataBaseFF.Program.MainPrg", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html", null ],
    [ "GameDataBaseFF.Web.Models.MapperFactory", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_mapper_factory.html", null ],
    [ "ObservableObject", null, [
      [ "GameDataBaseFF.Data.Game", "class_game_data_base_f_f_1_1_data_1_1_game.html", null ],
      [ "GameDataBaseFF.WpfClient.VM.GameVM", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html", null ]
    ] ],
    [ "GameDataBaseFF.Repository.ParentRepo< Company >", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html", [
      [ "GameDataBaseFF.Repository.CompanyRepository", "class_game_data_base_f_f_1_1_repository_1_1_company_repository.html", null ]
    ] ],
    [ "GameDataBaseFF.Repository.ParentRepo< Game >", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html", [
      [ "GameDataBaseFF.Repository.GameRepository", "class_game_data_base_f_f_1_1_repository_1_1_game_repository.html", null ]
    ] ],
    [ "GameDataBaseFF.Repository.ParentRepo< Platform >", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html", [
      [ "GameDataBaseFF.Repository.PlatformRepository", "class_game_data_base_f_f_1_1_repository_1_1_platform_repository.html", null ]
    ] ],
    [ "GameDataBaseFF.Data.Platform", "class_game_data_base_f_f_1_1_data_1_1_platform.html", null ],
    [ "GameDataBaseFF.Logic.PlatformNumsByCompany", "class_game_data_base_f_f_1_1_logic_1_1_platform_nums_by_company.html", null ],
    [ "GameDataBaseFF.Web.Program", "class_game_data_base_f_f_1_1_web_1_1_program.html", null ],
    [ "RazorPage", null, [
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ],
      [ "AspNetCore.Views_Games_GamesDetails", "class_asp_net_core_1_1_views___games___games_details.html", null ],
      [ "AspNetCore.Views_Games_GamesEdit", "class_asp_net_core_1_1_views___games___games_edit.html", null ],
      [ "AspNetCore.Views_Games_GamesIndex", "class_asp_net_core_1_1_views___games___games_index.html", null ],
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ]
    ] ],
    [ "RazorPage< IEnumerable< GameDataBaseFF.Web.Models.Game >>", null, [
      [ "AspNetCore.Views_Games_GamesList", "class_asp_net_core_1_1_views___games___games_list.html", null ]
    ] ],
    [ "SimpleIoc", null, [
      [ "GameDataBaseFF.WpfClient.MyIoc", "class_game_data_base_f_f_1_1_wpf_client_1_1_my_ioc.html", null ],
      [ "GameWPF.MyIoc", "class_game_w_p_f_1_1_my_ioc.html", null ]
    ] ],
    [ "GameDataBaseFF.Web.Startup", "class_game_data_base_f_f_1_1_web_1_1_startup.html", null ],
    [ "ViewModelBase", null, [
      [ "GameDataBaseFF.WpfClient.VM.MainVM", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_main_v_m.html", null ],
      [ "GameWPF.VM.EditorViewModel", "class_game_w_p_f_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "GameWPF.VM.MainViewModel", "class_game_w_p_f_1_1_v_m_1_1_main_view_model.html", null ]
    ] ],
    [ "Window", null, [
      [ "GameWPF.MainWindow", "class_game_w_p_f_1_1_main_window.html", null ],
      [ "GameWPF.UI.GameEditorWindow", "class_game_w_p_f_1_1_u_i_1_1_game_editor_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "GameDataBaseFF.WpfClient.EditorWindow", "class_game_data_base_f_f_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "GameDataBaseFF.WpfClient.MainWindow", "class_game_data_base_f_f_1_1_wpf_client_1_1_main_window.html", null ]
    ] ]
];