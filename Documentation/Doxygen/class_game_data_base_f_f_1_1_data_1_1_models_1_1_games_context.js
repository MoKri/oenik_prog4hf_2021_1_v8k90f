var class_game_data_base_f_f_1_1_data_1_1_models_1_1_games_context =
[
    [ "GamesContext", "class_game_data_base_f_f_1_1_data_1_1_models_1_1_games_context.html#a8bb31a9008cb4d9b71bbf5bae8f13a41", null ],
    [ "OnConfiguring", "class_game_data_base_f_f_1_1_data_1_1_models_1_1_games_context.html#a5250cb062b65958d04b5b622b6b51213", null ],
    [ "OnModelCreating", "class_game_data_base_f_f_1_1_data_1_1_models_1_1_games_context.html#a540db48ee9e2cb8f65927b3659198dbe", null ],
    [ "Companies", "class_game_data_base_f_f_1_1_data_1_1_models_1_1_games_context.html#a9d7d9a8948858f60cfcded0bb1257c3a", null ],
    [ "Games", "class_game_data_base_f_f_1_1_data_1_1_models_1_1_games_context.html#a4ea0638570b75591483dda5acbb62616", null ],
    [ "Platforms", "class_game_data_base_f_f_1_1_data_1_1_models_1_1_games_context.html#a85f6b7de2958c57c9bf6e86e5c65c1e4", null ]
];