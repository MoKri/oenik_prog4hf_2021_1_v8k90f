var interface_game_w_p_f_1_1_b_l_1_1_i_game_logic =
[
    [ "AddGame", "interface_game_w_p_f_1_1_b_l_1_1_i_game_logic.html#a599e3cbeb7a9af7227a7a0b825ec77d2", null ],
    [ "DeleteGame", "interface_game_w_p_f_1_1_b_l_1_1_i_game_logic.html#a9d0dbdaa840e3069ae712f5e0a5e790e", null ],
    [ "GetAllGames", "interface_game_w_p_f_1_1_b_l_1_1_i_game_logic.html#a21bc7b14df96c49729ea0c7df3792fc4", null ],
    [ "ModifyGame", "interface_game_w_p_f_1_1_b_l_1_1_i_game_logic.html#afb7e92166b058f08048547e4135c5b06", null ]
];