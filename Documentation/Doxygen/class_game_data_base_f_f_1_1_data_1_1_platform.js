var class_game_data_base_f_f_1_1_data_1_1_platform =
[
    [ "Platform", "class_game_data_base_f_f_1_1_data_1_1_platform.html#afcc442c80fe76ab7b26051d7fdef40a0", null ],
    [ "ToString", "class_game_data_base_f_f_1_1_data_1_1_platform.html#aaeea4240e86e9b5176fe6ee8de0532ad", null ],
    [ "Games", "class_game_data_base_f_f_1_1_data_1_1_platform.html#af6f0e69ebeb72d07e765a44b03a8f112", null ],
    [ "HDMI", "class_game_data_base_f_f_1_1_data_1_1_platform.html#a8efd4475095ad8024c4091ad498962eb", null ],
    [ "Id", "class_game_data_base_f_f_1_1_data_1_1_platform.html#a7db444bc6676490379ef41a726933407", null ],
    [ "Name", "class_game_data_base_f_f_1_1_data_1_1_platform.html#a1a4ec5442a71dcb1b87d3699a1e584fc", null ],
    [ "Price", "class_game_data_base_f_f_1_1_data_1_1_platform.html#acc992a171362085bc4a6e0bf015d6bee", null ],
    [ "Producer", "class_game_data_base_f_f_1_1_data_1_1_platform.html#ad3567d59a282aec571abb701601ee000", null ],
    [ "ProducerId", "class_game_data_base_f_f_1_1_data_1_1_platform.html#ac2bd51c4dfb195713cef7cb0a1cea0ca", null ],
    [ "ReleaseDate", "class_game_data_base_f_f_1_1_data_1_1_platform.html#ab7f2ad45cf866d52e9e9f8020ae286ca", null ],
    [ "Storage", "class_game_data_base_f_f_1_1_data_1_1_platform.html#abc32d41a5106875461349fb8b2ffe176", null ]
];