var namespace_game_data_base_f_f_1_1_logic =
[
    [ "Tests", "namespace_game_data_base_f_f_1_1_logic_1_1_tests.html", "namespace_game_data_base_f_f_1_1_logic_1_1_tests" ],
    [ "AveragePlatCostByComp", "class_game_data_base_f_f_1_1_logic_1_1_average_plat_cost_by_comp.html", "class_game_data_base_f_f_1_1_logic_1_1_average_plat_cost_by_comp" ],
    [ "DevelopmentLogic", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html", "class_game_data_base_f_f_1_1_logic_1_1_development_logic" ],
    [ "DeviceProduceLogic", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic" ],
    [ "GameByCompany", "class_game_data_base_f_f_1_1_logic_1_1_game_by_company.html", "class_game_data_base_f_f_1_1_logic_1_1_game_by_company" ],
    [ "GamePriceByCompany", "class_game_data_base_f_f_1_1_logic_1_1_game_price_by_company.html", "class_game_data_base_f_f_1_1_logic_1_1_game_price_by_company" ],
    [ "IDevelopmentLogic", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic" ],
    [ "IDeviceProduceLogic", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic" ],
    [ "PlatformNumsByCompany", "class_game_data_base_f_f_1_1_logic_1_1_platform_nums_by_company.html", "class_game_data_base_f_f_1_1_logic_1_1_platform_nums_by_company" ]
];