var namespace_asp_net_core =
[
    [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
    [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
    [ "Views_Games_GamesDetails", "class_asp_net_core_1_1_views___games___games_details.html", "class_asp_net_core_1_1_views___games___games_details" ],
    [ "Views_Games_GamesEdit", "class_asp_net_core_1_1_views___games___games_edit.html", "class_asp_net_core_1_1_views___games___games_edit" ],
    [ "Views_Games_GamesIndex", "class_asp_net_core_1_1_views___games___games_index.html", "class_asp_net_core_1_1_views___games___games_index" ],
    [ "Views_Games_GamesList", "class_asp_net_core_1_1_views___games___games_list.html", "class_asp_net_core_1_1_views___games___games_list" ],
    [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
    [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
    [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
    [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
    [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
];