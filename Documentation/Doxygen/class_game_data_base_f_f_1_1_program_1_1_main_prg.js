var class_game_data_base_f_f_1_1_program_1_1_main_prg =
[
    [ "AveragePrice", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a41233099079fe8bb6a1ae5fc73cfc077", null ],
    [ "CheapAvgPlat", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#ad71e798cb233b86a1ba1c7dfdd0ee81e", null ],
    [ "CheapAvgPlatAsync", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a90f2756480c2cc7c205c28d957142f0c", null ],
    [ "CompanyMenu", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a1898267b9e86eb8858923a7c5b2d78d5", null ],
    [ "GamesByCompanies", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a50dadd6c49d9cdfae32c41291701c8d7", null ],
    [ "GamesByCompaniesAsync", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a728ef6b3b0ce4928319e595a075cccca", null ],
    [ "GamesMenu", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a0afec7e82c8bbeab78e9f12f066b75ae", null ],
    [ "ListAll", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a5e16a06ee3739c3b244faab786b917f6", null ],
    [ "ListCompanies", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#ae134c5bc7ed534f84b4ffd0d9d0a4bc8", null ],
    [ "ListGames", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a5a9f4a71884e7121b6830b2f71e7015d", null ],
    [ "ListPlatforms", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a952cf103466f505cd5382d05d2e48861", null ],
    [ "Main", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a17c4d5f27237879f1de7d66a0ac6370a", null ],
    [ "NoHDMI", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#af80571217de5eed908d43302eb2e0f5a", null ],
    [ "NoHDMIAsync", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a909fc610982ace3ed3d2576e2c415246", null ],
    [ "PlatformMenu", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#aface8159b1b44471ee9b13c8920ea68d", null ],
    [ "PlatNumByComp", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#ac40874ca13074ff16971c5da28de8c52", null ],
    [ "PlatNumByCompAsync", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#a262e498e71bf98705543901ef1db63a7", null ],
    [ "SumGameByComp", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#ae4300e854ba433c653f8dcc4763e633b", null ],
    [ "SumGameByCompAsync", "class_game_data_base_f_f_1_1_program_1_1_main_prg.html#aa2a14d98c3c5ce37525c1b751fe239ad", null ]
];