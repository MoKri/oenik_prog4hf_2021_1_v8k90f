var class_game_data_base_f_f_1_1_repository_1_1_game_repository =
[
    [ "GameRepository", "class_game_data_base_f_f_1_1_repository_1_1_game_repository.html#ad57537f123244e54d89d809697eac57e", null ],
    [ "ChangeGame", "class_game_data_base_f_f_1_1_repository_1_1_game_repository.html#a192604d0eedd4326d9b01611cd788b72", null ],
    [ "ChangePrice", "class_game_data_base_f_f_1_1_repository_1_1_game_repository.html#a50019a4f05e717231421c2a8145006d0", null ],
    [ "GetOne", "class_game_data_base_f_f_1_1_repository_1_1_game_repository.html#ad5f7b08d330a7ed8dc615fff336f4127", null ],
    [ "Insert", "class_game_data_base_f_f_1_1_repository_1_1_game_repository.html#ac1053ad692b9ed9e7a7d5c4bdb65a7e0", null ],
    [ "Remove", "class_game_data_base_f_f_1_1_repository_1_1_game_repository.html#a078bb59f53511f25d32ba3f58962b9b2", null ]
];