var class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_development_logic_tests =
[
    [ "TestAddingANewCompany", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_development_logic_tests.html#aef73085b92336efd03eade77b7327ac0", null ],
    [ "TestGettingOnlyOneGame", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_development_logic_tests.html#a2224a879ff692d57c2340fb1c9815b78", null ],
    [ "TestThatGameAverageIsAsExpected", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_development_logic_tests.html#a39e1dbae539ec780c3492a7850b6d97d", null ],
    [ "TestThatGamesAreCorrectlyListedWithTheirDevelopmentCompany", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_development_logic_tests.html#a9cca6ef766ec9934c7523d7cee2d6c02", null ],
    [ "TestThatRemovesAGame", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_development_logic_tests.html#ad75c3f9db41dd9248061e136634f3c55", null ],
    [ "TestThatSumGamesByCompanyReturnsTheRightSums", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_development_logic_tests.html#a1000aa98304796831b6bff2999f112f2", null ]
];