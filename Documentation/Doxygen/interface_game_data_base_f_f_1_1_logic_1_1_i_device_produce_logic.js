var interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic =
[
    [ "ChangeCompanyBoss", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html#a598dbfc4ec11238f286396065aeb323a", null ],
    [ "ChangePlatformStorage", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html#a8e3d0cdbda96fe00306345c25b5f43dd", null ],
    [ "CreateCompany", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html#a4f0aefca21f1434a505b4b80636680c3", null ],
    [ "CreatePlatform", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html#aac130bb55bdd36d8d9a6ed442f05006e", null ],
    [ "GetAllCompanies", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html#a784e5dc12ea1848b43c45bfb1c224391", null ],
    [ "GetAllPlatforms", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html#a93a16ec13b849850acede81b0f694f94", null ],
    [ "GetOneCompany", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html#a66be44ee04cf32b37d7410423bb1f31c", null ],
    [ "GetOnePlatform", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html#a62b7b7c1f52c73584960d14d695a5346", null ],
    [ "RemoveCompany", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html#aba87010ae4a4908c14a18d21e3a6d308", null ],
    [ "RemovePlatform", "interface_game_data_base_f_f_1_1_logic_1_1_i_device_produce_logic.html#abed874b299ea62a2b3eacfbd8cf3cd52", null ]
];