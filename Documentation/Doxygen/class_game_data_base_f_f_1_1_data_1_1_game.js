var class_game_data_base_f_f_1_1_data_1_1_game =
[
    [ "Game", "class_game_data_base_f_f_1_1_data_1_1_game.html#a6bfbded56567c67c9a2e4c4c1d2611b8", null ],
    [ "CopyFrom", "class_game_data_base_f_f_1_1_data_1_1_game.html#a5e4f73d25a5f965b0f3121f3a5cd7142", null ],
    [ "ToString", "class_game_data_base_f_f_1_1_data_1_1_game.html#a183d5cae8d7dd6677199fcfe84ae9366", null ],
    [ "Company", "class_game_data_base_f_f_1_1_data_1_1_game.html#ae4cedb732d9917a74e1844e1c2866d6d", null ],
    [ "CompanyId", "class_game_data_base_f_f_1_1_data_1_1_game.html#a9899a00527146924a0403712a4bb0233", null ],
    [ "Genre", "class_game_data_base_f_f_1_1_data_1_1_game.html#ac436e760a68b48db4d90cc0bdabb27fa", null ],
    [ "Id", "class_game_data_base_f_f_1_1_data_1_1_game.html#ab621711ad3efa1b525a4a675f1a49036", null ],
    [ "Name", "class_game_data_base_f_f_1_1_data_1_1_game.html#ae161291b6ecadab378e4f72b066eb817", null ],
    [ "Platform", "class_game_data_base_f_f_1_1_data_1_1_game.html#a742fd968508e61ce78765dc78e2e0427", null ],
    [ "PlatformId", "class_game_data_base_f_f_1_1_data_1_1_game.html#a52cb4d9f5e803fba6d7bd2f79472e1cf", null ],
    [ "Price", "class_game_data_base_f_f_1_1_data_1_1_game.html#a528b25aa9dcdc90a58d2b5c852e3a64b", null ],
    [ "ReleaseDate", "class_game_data_base_f_f_1_1_data_1_1_game.html#a46f969664604b977c51f8e5c99ce7cd7", null ],
    [ "Type", "class_game_data_base_f_f_1_1_data_1_1_game.html#a4e59f3b57b0c226ae12832be7f39b6c8", null ]
];