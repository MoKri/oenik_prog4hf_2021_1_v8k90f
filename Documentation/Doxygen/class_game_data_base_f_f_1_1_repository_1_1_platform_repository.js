var class_game_data_base_f_f_1_1_repository_1_1_platform_repository =
[
    [ "PlatformRepository", "class_game_data_base_f_f_1_1_repository_1_1_platform_repository.html#a10d433ede5299266f18f988ef20b8e86", null ],
    [ "ChangeStorage", "class_game_data_base_f_f_1_1_repository_1_1_platform_repository.html#a601b50dee7f09b8e48eea215f1d5dffc", null ],
    [ "GetOne", "class_game_data_base_f_f_1_1_repository_1_1_platform_repository.html#ad3c85ebc95cbf70f062fb9110f41859b", null ],
    [ "Insert", "class_game_data_base_f_f_1_1_repository_1_1_platform_repository.html#aaec9f68a1df15508a027822752ecbbea", null ],
    [ "Remove", "class_game_data_base_f_f_1_1_repository_1_1_platform_repository.html#a58e4c80054c1b77c94f68e18af3d1b5c", null ]
];