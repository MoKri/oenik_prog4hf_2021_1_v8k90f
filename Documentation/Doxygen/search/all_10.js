var searchData=
[
  ['views_5f_5fviewimports_223',['Views__ViewImports',['../class_asp_net_core_1_1_views_____view_imports.html',1,'AspNetCore']]],
  ['views_5f_5fviewstart_224',['Views__ViewStart',['../class_asp_net_core_1_1_views_____view_start.html',1,'AspNetCore']]],
  ['views_5fgames_5fgamesdetails_225',['Views_Games_GamesDetails',['../class_asp_net_core_1_1_views___games___games_details.html',1,'AspNetCore']]],
  ['views_5fgames_5fgamesedit_226',['Views_Games_GamesEdit',['../class_asp_net_core_1_1_views___games___games_edit.html',1,'AspNetCore']]],
  ['views_5fgames_5fgamesindex_227',['Views_Games_GamesIndex',['../class_asp_net_core_1_1_views___games___games_index.html',1,'AspNetCore']]],
  ['views_5fgames_5fgameslist_228',['Views_Games_GamesList',['../class_asp_net_core_1_1_views___games___games_list.html',1,'AspNetCore']]],
  ['views_5fhome_5findex_229',['Views_Home_Index',['../class_asp_net_core_1_1_views___home___index.html',1,'AspNetCore']]],
  ['views_5fhome_5fprivacy_230',['Views_Home_Privacy',['../class_asp_net_core_1_1_views___home___privacy.html',1,'AspNetCore']]],
  ['views_5fshared_5f_5flayout_231',['Views_Shared__Layout',['../class_asp_net_core_1_1_views___shared_____layout.html',1,'AspNetCore']]],
  ['views_5fshared_5f_5fvalidationscriptspartial_232',['Views_Shared__ValidationScriptsPartial',['../class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html',1,'AspNetCore']]],
  ['views_5fshared_5ferror_233',['Views_Shared_Error',['../class_asp_net_core_1_1_views___shared___error.html',1,'AspNetCore']]]
];
