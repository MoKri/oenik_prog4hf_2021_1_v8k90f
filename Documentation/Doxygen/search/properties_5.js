var searchData=
[
  ['game_465',['Game',['../class_game_w_p_f_1_1_v_m_1_1_editor_view_model.html#a2d276ce0b144a10d4a67af6a2cf3d18e',1,'GameWPF::VM::EditorViewModel']]],
  ['games_466',['Games',['../class_game_data_base_f_f_1_1_data_1_1_company.html#a01afefa5180367f22d41d13eee3e1467',1,'GameDataBaseFF.Data.Company.Games()'],['../class_game_data_base_f_f_1_1_data_1_1_models_1_1_games_context.html#a4ea0638570b75591483dda5acbb62616',1,'GameDataBaseFF.Data.Models.GamesContext.Games()'],['../class_game_data_base_f_f_1_1_data_1_1_platform.html#af6f0e69ebeb72d07e765a44b03a8f112',1,'GameDataBaseFF.Data.Platform.Games()'],['../class_game_w_p_f_1_1_v_m_1_1_main_view_model.html#aa1f12140cdf5934081fceef0f5b219d6',1,'GameWPF.VM.MainViewModel.Games()']]],
  ['gametitles_467',['GameTitles',['../class_game_data_base_f_f_1_1_logic_1_1_game_by_company.html#a1f6df32ad77e692e540886a4aee97940',1,'GameDataBaseFF::Logic::GameByCompany']]],
  ['genre_468',['Genre',['../class_game_data_base_f_f_1_1_data_1_1_game.html#ac436e760a68b48db4d90cc0bdabb27fa',1,'GameDataBaseFF.Data.Game.Genre()'],['../class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html#a1bff6c7f2b869e8fc0d6ccac63b88b5e',1,'GameDataBaseFF.Web.Models.Game.Genre()'],['../class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html#af2cf960fa022c55c759a2d3efa5629e3',1,'GameDataBaseFF.WpfClient.VM.GameVM.Genre()']]]
];
