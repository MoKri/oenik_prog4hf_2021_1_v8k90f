var class_game_data_base_f_f_1_1_logic_1_1_development_logic =
[
    [ "DevelopmentLogic", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a72e952d25010d482913d36d38ae1cf6a", null ],
    [ "AverageGamePrice", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a73364b0db3ab091ad3635fbb12804868", null ],
    [ "ChangeCompanyBoss", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a14b86b967e98bc0390fd10384e471bd1", null ],
    [ "ChangeGame", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a2bd19af7584148176847a523ecf94787", null ],
    [ "ChangeGamePrice", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a7c57d938dfecb2ca4d64bb66adcda05e", null ],
    [ "CreateCompany", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a1280a77a382c20816f274446eae97a5a", null ],
    [ "CreateGame", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#ab36fadf53b7a4c5911cf372e4d04774a", null ],
    [ "GamesByCompanies", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#af3e1bdb9fdac012e7cdad80210a5e12a", null ],
    [ "GamesByCompaniesAsync", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a925e35f6129e24a113f6c20973ebd692", null ],
    [ "GetAllCompanies", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a4a45fd9a287a5fbc7825dac54b1f5595", null ],
    [ "GetAllGames", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#af3497acde77861e862f01940e7ba762e", null ],
    [ "GetOneCompany", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a7241549ac67c062b55db5a0897a5e8ff", null ],
    [ "GetOneGame", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a34a602171dba1517af7d9ac6fb232ea4", null ],
    [ "RemoveCompany", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#a215477b1c3d01f42261a4236a281d546", null ],
    [ "RemoveGame", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#ac857c89cabf0f21e441023feb310ab11", null ],
    [ "SumGamesPriceByCompany", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#ae7ac0f8e163a984c2a6ebae15fa7e745", null ],
    [ "SumGamesPriceByCompanyAsync", "class_game_data_base_f_f_1_1_logic_1_1_development_logic.html#ace7949a1748709d1aa3a0f51063a8bfe", null ]
];