var class_game_data_base_f_f_1_1_repository_1_1_parent_repo =
[
    [ "ParentRepo", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html#a2e4cea8f07549d52d5f64c7c2d308cba", null ],
    [ "GetAll", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html#a5a99d17c04dcc77908519874a5e5b7dd", null ],
    [ "GetOne", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html#adc57372f362a34ac99e6593874fe4e4d", null ],
    [ "Insert", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html#a14ca654805708b611c127040e866f9d0", null ],
    [ "Remove", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html#ad118fc835fac1278d703a5dfce142092", null ],
    [ "ctx", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html#aa525162249f00c65cf96d85c3a2a712e", null ]
];