var class_game_w_p_f_1_1_v_m_1_1_main_view_model =
[
    [ "MainViewModel", "class_game_w_p_f_1_1_v_m_1_1_main_view_model.html#aa57bc6a6ee14b052868184937b44ab31", null ],
    [ "MainViewModel", "class_game_w_p_f_1_1_v_m_1_1_main_view_model.html#afa074915e4d903923ce3481d586a8aab", null ],
    [ "AddCommand", "class_game_w_p_f_1_1_v_m_1_1_main_view_model.html#a369e506d05bb08549397d383c4bbe283", null ],
    [ "DelCommand", "class_game_w_p_f_1_1_v_m_1_1_main_view_model.html#aec747fb80a1bb7613e69fe9561c40788", null ],
    [ "Games", "class_game_w_p_f_1_1_v_m_1_1_main_view_model.html#aa1f12140cdf5934081fceef0f5b219d6", null ],
    [ "ModifyCommand", "class_game_w_p_f_1_1_v_m_1_1_main_view_model.html#aac72d26caf4bc697a0aeff346dd264cc", null ],
    [ "SelectedGame", "class_game_w_p_f_1_1_v_m_1_1_main_view_model.html#ae0ce25e04264f4ddbd1997b9141d9058", null ]
];