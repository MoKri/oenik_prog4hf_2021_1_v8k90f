var class_game_data_base_f_f_1_1_data_1_1_company =
[
    [ "Company", "class_game_data_base_f_f_1_1_data_1_1_company.html#ae73e07c8fac3660db0483c77d73fe655", null ],
    [ "Equals", "class_game_data_base_f_f_1_1_data_1_1_company.html#a4365795b3c112e8694ed02231d81d245", null ],
    [ "GetHashCode", "class_game_data_base_f_f_1_1_data_1_1_company.html#a8efe49c5bb42ce63e0e72703a451193f", null ],
    [ "ToString", "class_game_data_base_f_f_1_1_data_1_1_company.html#af6b9a8443b5730dfeddfa415a0047ab3", null ],
    [ "CEO", "class_game_data_base_f_f_1_1_data_1_1_company.html#af961f465a9513860f8645a19cdbd86e0", null ],
    [ "FoundDate", "class_game_data_base_f_f_1_1_data_1_1_company.html#a0545f60046beb4a88ba9b2d3f7d0dca8", null ],
    [ "Games", "class_game_data_base_f_f_1_1_data_1_1_company.html#a01afefa5180367f22d41d13eee3e1467", null ],
    [ "HeadquartersCity", "class_game_data_base_f_f_1_1_data_1_1_company.html#a4019561d844b5d68a36b6e081007491a", null ],
    [ "HeadquartersCountry", "class_game_data_base_f_f_1_1_data_1_1_company.html#a30222bcb98d82075a4a17afee017cb8e", null ],
    [ "Id", "class_game_data_base_f_f_1_1_data_1_1_company.html#a3d8e5873625216779a8e65951d9a46fa", null ],
    [ "Name", "class_game_data_base_f_f_1_1_data_1_1_company.html#a90b9e3b86a621da4c8b0ab1ea55f122a", null ],
    [ "Platforms", "class_game_data_base_f_f_1_1_data_1_1_company.html#a4cf917c568982e46de9f78b8ff6dcc20", null ]
];