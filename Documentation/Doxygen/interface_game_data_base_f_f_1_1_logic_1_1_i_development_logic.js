var interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic =
[
    [ "ChangeCompanyBoss", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#ac58f8fb737fe5353bb85070ebe264c89", null ],
    [ "ChangeGame", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#a688555b86376a9e5c396966f4a8cc0d3", null ],
    [ "ChangeGamePrice", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#a86307eeba9b119d540c9505a06182852", null ],
    [ "CreateCompany", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#aa47d13824274de8cfde094062251801b", null ],
    [ "CreateGame", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#afd42c0d24643559235391ef28468ed7d", null ],
    [ "GetAllCompanies", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#abe4a40f7f2f59e4f382aabfd9ec9f160", null ],
    [ "GetAllGames", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#a6e76b0a3fee7ffcc5e0579c2af9b73f3", null ],
    [ "GetOneCompany", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#aed5010920f5976f1ad9606cdb13128e4", null ],
    [ "GetOneGame", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#ad87696a2b2631146fa92e2cd0b098cc7", null ],
    [ "RemoveCompany", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#a242763adcc755ef97bf06362bbb58d16", null ],
    [ "RemoveGame", "interface_game_data_base_f_f_1_1_logic_1_1_i_development_logic.html#a4d0929fd94a9590de988daa1ee6f164e", null ]
];