var dir_d51a7ee88a7068e9c73d84e51ddcfbe6 =
[
    [ "GameDataBaseFF.Data", "dir_fe07c69a9815e726f148fb45534ffbff.html", "dir_fe07c69a9815e726f148fb45534ffbff" ],
    [ "GameDataBaseFF.Logic", "dir_e2a79db4f57d6943d5bbdab6c01d0cda.html", "dir_e2a79db4f57d6943d5bbdab6c01d0cda" ],
    [ "GameDataBaseFF.Logic.Tests", "dir_cc9f8d82bf35440d56108df7e9197f75.html", "dir_cc9f8d82bf35440d56108df7e9197f75" ],
    [ "GameDataBaseFF.Program", "dir_08e47a7394f05b064380e01bff974ba1.html", "dir_08e47a7394f05b064380e01bff974ba1" ],
    [ "GameDataBaseFF.Repository", "dir_f2cfe1e2559dda87c767e50a7405c625.html", "dir_f2cfe1e2559dda87c767e50a7405c625" ],
    [ "GameDataBaseFF.Web", "dir_b41719ff0cecd5066265b15cc8edf7a9.html", "dir_b41719ff0cecd5066265b15cc8edf7a9" ],
    [ "GameDataBaseFF.WpfClient", "dir_76419a6c68c2daf405f41940bdb8352d.html", "dir_76419a6c68c2daf405f41940bdb8352d" ],
    [ "GameWPF", "dir_b2b1703050c8d46b30c246d8703cec59.html", "dir_b2b1703050c8d46b30c246d8703cec59" ]
];