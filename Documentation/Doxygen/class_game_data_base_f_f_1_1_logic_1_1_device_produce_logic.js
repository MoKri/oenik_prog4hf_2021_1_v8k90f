var class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic =
[
    [ "DeviceProduceLogic", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#a9392643895caca028a474aa7f551282e", null ],
    [ "AVGPlatformCostByCompany", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#a6fe9a8aac9f66f8094cd336d96e260aa", null ],
    [ "AVGPlatformCostByCompanyAsync", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#a026885c32ccafd9acc08e52436c11dd9", null ],
    [ "ChangeCompanyBoss", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#aba28369704a216d82ccbbecd45d6abba", null ],
    [ "ChangePlatformStorage", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#a1922f9f126ed839fa99ce6690483e9b2", null ],
    [ "CountPlatformByCompany", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#aa4344e6b5965a19b79c6beca3893c19c", null ],
    [ "CountPlatformByCompanyAsync", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#afb1a4142889b3ba3b9736fa0f132677a", null ],
    [ "CreateCompany", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#ab4a9ed80fb1cee25a9a6c3d5787ecd30", null ],
    [ "CreatePlatform", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#aad3b1de01582b0b305659c90a6b3dfa6", null ],
    [ "GetAllCompanies", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#aeb3c79bacaf6cf6d7349cb9705459d6d", null ],
    [ "GetAllPlatforms", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#a563698207cd34df7e3de4e2d91df7d66", null ],
    [ "GetOneCompany", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#a939a978b550de199cac5d871315b0c46", null ],
    [ "GetOnePlatform", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#a100c95670ad0c15dd68f7993f6f988ea", null ],
    [ "NonHDMIPlatforms", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#a09ddb591eedbd27e06b5e6f46d15ab1c", null ],
    [ "NonHDMIPlatformsAsync", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#a88ffcc44e50d9763737266d7f92b29e6", null ],
    [ "RemoveCompany", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#a29f3dc1e18378de2c9f46c64013b0498", null ],
    [ "RemovePlatform", "class_game_data_base_f_f_1_1_logic_1_1_device_produce_logic.html#aa4bab512d5b833ff69d984bf447855b7", null ]
];