var namespace_game_data_base_f_f_1_1_web_1_1_models =
[
    [ "ErrorViewModel", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_error_view_model.html", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_error_view_model" ],
    [ "Game", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game" ],
    [ "GameListViewModel", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game_list_view_model.html", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game_list_view_model" ],
    [ "MapperFactory", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_mapper_factory.html", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_mapper_factory" ]
];