var class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_device_produce_logic_tests =
[
    [ "TestThatAVGCostByCompanyReturnsTheCompanyWithTheCheapestAverage", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_device_produce_logic_tests.html#aaf432d3dce9d6c880e2ab3d455afc637", null ],
    [ "TestThatCountPlatformByCompanyReturnsTheRightAmounts", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_device_produce_logic_tests.html#a7a17272937b0c3ca96eccb09bbf672f1", null ],
    [ "TestThatGetsAllOfThePlatforms", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_device_produce_logic_tests.html#a6bc45466fb8d8ff6c49ea635633aba36", null ],
    [ "TestThatNonHDMIOnlyReturnsPlatformsWithNoHDMIPort", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_device_produce_logic_tests.html#a9e5a3e11f3e9ebfd6412431b04b8271a", null ],
    [ "TestThatUpdatesACompanysCEO", "class_game_data_base_f_f_1_1_logic_1_1_tests_1_1_device_produce_logic_tests.html#ab04124302f372901d9a0449d104a3ed6", null ]
];