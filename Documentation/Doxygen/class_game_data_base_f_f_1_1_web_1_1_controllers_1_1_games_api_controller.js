var class_game_data_base_f_f_1_1_web_1_1_controllers_1_1_games_api_controller =
[
    [ "GamesApiController", "class_game_data_base_f_f_1_1_web_1_1_controllers_1_1_games_api_controller.html#aa54d8bfb688fc93aadfd5acd69b63227", null ],
    [ "AddOneGame", "class_game_data_base_f_f_1_1_web_1_1_controllers_1_1_games_api_controller.html#a70f3dec383ad1d45c2c9f1ab68030c87", null ],
    [ "DeleteOneGame", "class_game_data_base_f_f_1_1_web_1_1_controllers_1_1_games_api_controller.html#a713e97fe3ae5e3012905e0142fbb0f0f", null ],
    [ "GetAll", "class_game_data_base_f_f_1_1_web_1_1_controllers_1_1_games_api_controller.html#ad44d3bc7c76541afdce5fe802684a581", null ],
    [ "ModifyOneGame", "class_game_data_base_f_f_1_1_web_1_1_controllers_1_1_games_api_controller.html#a16f479cf9e538eb79124995c1841fb29", null ]
];