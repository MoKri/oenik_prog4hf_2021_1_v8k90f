var class_game_w_p_f_1_1_b_l_1_1_game_logic =
[
    [ "GameLogic", "class_game_w_p_f_1_1_b_l_1_1_game_logic.html#a7535d2945a5512371460f395906f3593", null ],
    [ "AddGame", "class_game_w_p_f_1_1_b_l_1_1_game_logic.html#ac4454af7afbe92b67139f8ea46efe504", null ],
    [ "DeleteGame", "class_game_w_p_f_1_1_b_l_1_1_game_logic.html#a84484d9b1749635c327feb19743c4773", null ],
    [ "GetAllGames", "class_game_w_p_f_1_1_b_l_1_1_game_logic.html#a13f7309fca2cc14546197929d54af336", null ],
    [ "ModifyGame", "class_game_w_p_f_1_1_b_l_1_1_game_logic.html#a31c8d6840de00638a54d1648e1974aea", null ]
];