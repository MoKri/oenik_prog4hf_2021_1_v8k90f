var class_game_data_base_f_f_1_1_web_1_1_models_1_1_game =
[
    [ "CompanyName", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html#a6eaa7e541d8b3123c1c90a48f70d7326", null ],
    [ "Genre", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html#a1bff6c7f2b869e8fc0d6ccac63b88b5e", null ],
    [ "ID", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html#ac56f31e3831b527613262d7fdebb3429", null ],
    [ "Name", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html#a97b6308920a1bc7fab115773e8afeff4", null ],
    [ "PlatformName", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html#a90facc3a009a279011aee34c44ec4b84", null ],
    [ "Price", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html#a03fda159e7932f878a9df96bc1193eda", null ],
    [ "ReleaseDate", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html#ad288c57bb387be3f3aebcde88130b37b", null ],
    [ "Type", "class_game_data_base_f_f_1_1_web_1_1_models_1_1_game.html#afd9b2925ab59b253836b94235abfad19", null ]
];