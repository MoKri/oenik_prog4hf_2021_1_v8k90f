var namespace_game_data_base_f_f_1_1_repository =
[
    [ "CompanyRepository", "class_game_data_base_f_f_1_1_repository_1_1_company_repository.html", "class_game_data_base_f_f_1_1_repository_1_1_company_repository" ],
    [ "GameRepository", "class_game_data_base_f_f_1_1_repository_1_1_game_repository.html", "class_game_data_base_f_f_1_1_repository_1_1_game_repository" ],
    [ "ICompanyRepository", "interface_game_data_base_f_f_1_1_repository_1_1_i_company_repository.html", "interface_game_data_base_f_f_1_1_repository_1_1_i_company_repository" ],
    [ "IGameRepository", "interface_game_data_base_f_f_1_1_repository_1_1_i_game_repository.html", "interface_game_data_base_f_f_1_1_repository_1_1_i_game_repository" ],
    [ "IPlatformRepository", "interface_game_data_base_f_f_1_1_repository_1_1_i_platform_repository.html", "interface_game_data_base_f_f_1_1_repository_1_1_i_platform_repository" ],
    [ "IRepository", "interface_game_data_base_f_f_1_1_repository_1_1_i_repository.html", "interface_game_data_base_f_f_1_1_repository_1_1_i_repository" ],
    [ "ParentRepo", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo.html", "class_game_data_base_f_f_1_1_repository_1_1_parent_repo" ],
    [ "PlatformRepository", "class_game_data_base_f_f_1_1_repository_1_1_platform_repository.html", "class_game_data_base_f_f_1_1_repository_1_1_platform_repository" ]
];