var class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m =
[
    [ "CopyFrom", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html#a2d99c31e0f03fe5986ab3b37f97d981b", null ],
    [ "CompanyName", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html#a5b659ef5290069c499aa2182b8012a90", null ],
    [ "Genre", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html#af2cf960fa022c55c759a2d3efa5629e3", null ],
    [ "Id", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html#a302996e3d9ccc98416702833a1b9042c", null ],
    [ "Name", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html#ada445b132856f92f9142e53b7b5616d1", null ],
    [ "PlatformName", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html#aea76fe7da6348bd531e4b887a897399f", null ],
    [ "Price", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html#af0a31416d3838a863e673d9ac442b3ac", null ],
    [ "Release", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html#a3f47d1156f11515cc4afecbb471044e4", null ],
    [ "Type", "class_game_data_base_f_f_1_1_wpf_client_1_1_v_m_1_1_game_v_m.html#a2c013e83437d63a672b43c4748b2ca5c", null ]
];