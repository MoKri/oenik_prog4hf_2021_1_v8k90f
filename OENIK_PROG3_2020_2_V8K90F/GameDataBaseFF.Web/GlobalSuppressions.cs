﻿// <copyright file="GlobalSuppressions.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "The list has to settable.", Scope = "member", Target = "~P:GameDataBaseFF.Web.Models.GameListViewModel.ListOfGames")]
[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("StyleCop.CSharp.NamingRules", "SA1309:Field names should not begin with underscore", Justification = "This was automatically generated.", Scope = "member", Target = "~F:GameDataBaseFF.Web.Controllers.HomeController._logger")]
[assembly: SuppressMessage("Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "This was automatically generated.", Scope = "type", Target = "~T:GameDataBaseFF.Web.Program")]
[assembly: SuppressMessage("Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "I think the name is good like that.", Scope = "type", Target = "~T:GameDataBaseFF.Web.Models.MapperFactory")]
