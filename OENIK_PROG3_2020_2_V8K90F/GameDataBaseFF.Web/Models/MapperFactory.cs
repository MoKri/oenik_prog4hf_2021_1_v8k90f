﻿// <copyright file="MapperFactory.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// This will be the mapper class.
    /// </summary>
    public class MapperFactory
    {
        /// <summary>
        /// Creates mappers for the game entitites.
        /// </summary>
        /// <returns>The configures mapped configuration.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<GameDataBaseFF.Data.Game, GameDataBaseFF.Web.Models.Game>()
                        .ForMember(dest => dest.CompanyName, map => map.MapFrom(src => src.Company == null ? string.Empty : src.Company.Name))
                        .ForMember(dest => dest.PlatformName, map => map.MapFrom(src => src.Platform == null ? string.Empty : src.Platform.Name));
            });

            return config.CreateMapper();
        }
    }
}
