﻿// <copyright file="GameListViewModel.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// This will be used for displaying the data on the webpage.
    /// </summary>
    public class GameListViewModel
    {
        /// <summary>
        /// Gets or Sets the list of games which are in the database.
        /// </summary>
        public IList<Game> ListOfGames { get; set; }

        /// <summary>
        /// Gets or Sets the game which can be edited on the webpage.
        /// </summary>
        public Game EditableGame { get; set; }
    }
}
