// <copyright file="ErrorViewModel.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Web.Models
{
    using System;

    /// <summary>
    /// The error view model for the mvc.
    /// </summary>
    public class ErrorViewModel
    {
        /// <summary>
        /// Gets or Sets the request id.
        /// </summary>
        public string RequestId { get; set; }

        /// <summary>
        /// Gets a value indicating whether the request id is empty or null.
        /// </summary>
        public bool ShowRequestId => !string.IsNullOrEmpty(this.RequestId);
    }
}
