﻿// <copyright file="Game.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Web.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// This will be used for the Web page to know what properties the game has.
    /// </summary>
    public class Game
    {
        /// <summary>
        /// Gets or Sets the ID of the Game.
        /// </summary>
        [Display(Name = "Game ID")]
        [Required]
        public int ID { get; set; }

        /// <summary>
        /// Gets or Sets the Name of the Game.
        /// </summary>
        [Display(Name = "Name")]
        [Required]
        [StringLength(120, MinimumLength = 1)]
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets the release date of the Game.
        /// </summary>
        [Display(Name = "Release date")]
        [Required]
        public DateTime ReleaseDate { get; set; }

        /// <summary>
        /// Gets or Sets the price of the Game in HUF.
        /// </summary>
        [Display(Name = "Price")]
        [Required]
        public int Price { get; set; }

        /// <summary>
        /// Gets or Sets the genre of the Game.
        /// </summary>
        [Display(Name = "Game genre")]
        [Required]
        [StringLength(120)]
        public string Genre { get; set; }

        /// <summary>
        /// Gets or Sets the type of the Game.
        /// </summary>
        [Display(Name = "Game type")]
        [Required]
        [StringLength(3, MinimumLength = 2)]
        public string Type { get; set; }

        /// <summary>
        /// Gets or Sets the name of the company who made the Game.
        /// </summary>
        [Display(Name = "Company name")]
        [Required]
        [StringLength(80, MinimumLength = 1)]
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or Sets the platform where the Game released.
        /// </summary>
        [Display(Name = "Platform name")]
        [Required]
        [StringLength(50, MinimumLength = 1)]
        public string PlatformName { get; set; }
    }
}
