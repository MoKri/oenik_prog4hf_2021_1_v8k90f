﻿// <copyright file="GamesController.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using GameDataBaseFF.Logic;
    using GameDataBaseFF.Web.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// GamesController class this will be dealing with the requests.
    /// </summary>
    public class GamesController : Controller
    {
        private IDevelopmentLogic logic;
        private IDeviceProduceLogic pLogic;
        private IMapper mapper;
        private GameListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="GamesController"/> class.
        /// </summary>
        /// <param name="logic">Development logic.</param>
        /// <param name="plogic">Deviceproduce logic.</param>
        /// <param name="mapper">Mapper.</param>
        public GamesController(IDevelopmentLogic logic, IDeviceProduceLogic plogic, IMapper mapper)
        {
            this.logic = logic;
            this.mapper = mapper;
            this.pLogic = plogic;

            this.vm = new GameListViewModel() { EditableGame = new GameDataBaseFF.Web.Models.Game(), ListOfGames = this.mapper.Map<IList<GameDataBaseFF.Data.Game>, List<GameDataBaseFF.Web.Models.Game>>(this.logic.GetAllGames()) };
        }

        /// <summary>
        /// Sends back the main view.
        /// </summary>
        /// <returns>The GAmesIndex page.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "addNew";
            return this.View("GamesIndex", this.vm);
        }

        /// <summary>
        /// Method for details of the games.
        /// </summary>
        /// <param name="id">The gmae id which we want to see.</param>
        /// <returns>Returns the page of a specific game.</returns>
        public IActionResult Details(int id)
        {
            return this.View("GamesDetails", this.GetGameModel(id));
        }

        /// <summary>
        /// Deletes a game from the database.
        /// </summary>
        /// <param name="id">the game we want to delete.</param>
        /// <returns>A page where the game will be deleted.</returns>
        public IActionResult Delete(int id)
        {
            this.TempData["editResult"] = "Delete Failed!";
            if (this.logic.RemoveGame(this.logic.GetOneGame(id)))
            {
                this.TempData["editResult"] = "Delete Succesful!";
            }

            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Modifies a game.
        /// </summary>
        /// <param name="id">the game id which we want to modify.</param>
        /// <returns>A page where the game will be modified.</returns>
        public IActionResult Modify(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditableGame = this.GetGameModel(id);
            return this.View("GamesIndex", this.vm);
        }

        /// <summary>
        /// The modification of an existing, and adding of a new game.
        /// </summary>
        /// <param name="game">The game we want to modify.</param>
        /// <param name="editAction">Whether we want to modify or create.</param>
        /// <returns>A page with the modified game in the database.</returns>
        [HttpPost]
        public IActionResult Modify(GameDataBaseFF.Web.Models.Game game, string editAction)
        {
            int addableID = 0;
            int paddableID = 0;
            if (this.ModelState.IsValid && game != null)
            {
                if (this.logic.GetAllCompanies().Any(c => c.Name == game.CompanyName))
                {
                    addableID = this.logic.GetAllCompanies().Where(c => c.Name == game.CompanyName).First().Id;
                }

                if (this.pLogic.GetAllPlatforms().Any(p => p.Name == game.PlatformName))
                {
                    paddableID = this.pLogic.GetAllPlatforms().Where(p => p.Name == game.PlatformName).First().Id;
                }

                if (addableID == 0)
                {
                    addableID = 1;
                }

                if (paddableID == 0)
                {
                    paddableID = 1;
                }

                this.TempData["editResult"] = "Nothing Happened!";
                if (editAction == "addNew")
                {
                    try
                    {
                        this.logic.CreateGame(game.Name, game.ReleaseDate, game.Price, game.Genre, game.Type, addableID, paddableID);
                        this.TempData["editResult"] = "Edit Succesful!";
                    }
                    catch (ArgumentException aex)
                    {
                        this.TempData["editResult"] = "Edit Failed!" + aex.Message;
                    }
                }
                else
                {
                    this.TempData["editResult"] = "Else Happened!";
                    if (this.logic.ChangeGame(game.ID, game.Name, game.ReleaseDate, game.Genre, game.Type, game.Price, addableID, paddableID))
                    {
                        this.vm.ListOfGames = this.mapper.Map<IList<GameDataBaseFF.Data.Game>, List<GameDataBaseFF.Web.Models.Game>>(this.logic.GetAllGames());
                        this.TempData["editResult"] = "Edit Succesful!";
                    }
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditableGame = game;
                return this.View("GamesIndex", this.vm);
            }
        }

        private GameDataBaseFF.Web.Models.Game GetGameModel(int id)
        {
            GameDataBaseFF.Data.Game selected = this.logic.GetOneGame(id);
            return this.mapper.Map<GameDataBaseFF.Data.Game, GameDataBaseFF.Web.Models.Game>(selected);
        }
    }
}
