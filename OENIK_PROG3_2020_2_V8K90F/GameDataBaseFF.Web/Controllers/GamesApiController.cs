﻿// <copyright file="GamesApiController.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Web.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using GameDataBaseFF.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Api controller.
    /// </summary>
    public class GamesApiController : Controller
    {
        private IDevelopmentLogic devLogic;
        private IDeviceProduceLogic devProdLogic;
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="GamesApiController"/> class.
        /// </summary>
        /// <param name="devLogic">Development logic.</param>
        /// <param name="devProdLogic">DeviceProduceLogic.</param>
        /// <param name="mapper">Mapper.</param>
        public GamesApiController(IDevelopmentLogic devLogic, IDeviceProduceLogic devProdLogic, IMapper mapper)
        {
            this.devLogic = devLogic;
            this.devProdLogic = devProdLogic;
            this.mapper = mapper;
        }

        /// <summary>
        /// Gets all the games.
        /// </summary>
        /// <returns>A list of games.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Game> GetAll()
        {
            var games = this.devLogic.GetAllGames();
            return this.mapper.Map<IList<Data.Game>, List<Models.Game>>(games);
        }

        /// <summary>
        /// Deletes a game.
        /// </summary>
        /// <param name="id">The id of the game we want to delete.</param>
        /// <returns>Whether the game was deleted or not.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DeleteOneGame(int id)
        {
            return new ApiResult() { OperationResult = this.devLogic.RemoveGame(this.devLogic.GetOneGame(id)) };
        }

        /// <summary>
        /// Adds a game to the database.
        /// </summary>
        /// <param name="game">The game we want to add.</param>
        /// <returns>Whether the adding was successful.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneGame(Models.Game game)
        {
            bool success = true;
            var comp = this.devLogic.GetAllCompanies().Where(c => c.Name == game.CompanyName).FirstOrDefault();
            var plat = this.devProdLogic.GetAllPlatforms().Where(p => p.Name == game.PlatformName).FirstOrDefault();

            if (comp != null && plat != null && game != null)
            {
                this.devLogic.CreateGame(game.Name, game.ReleaseDate, game.Price, game.Genre, game.Type, comp.Id, plat.Id);
            }
            else
            {
                success = false;
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// Modifies a game.
        /// </summary>
        /// <param name="game">The game we want to modify.</param>
        /// <returns>Whether the modify was successful.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModifyOneGame(Models.Game game)
        {
            var comp = this.devLogic.GetAllCompanies().Where(c => c.Name == game.CompanyName).FirstOrDefault();
            var plat = this.devProdLogic.GetAllPlatforms().Where(p => p.Name == game.PlatformName).FirstOrDefault();

            if (comp != null && plat != null && game != null)
            {
                return new ApiResult() { OperationResult = this.devLogic.ChangeGame(game.ID, game.Name, game.ReleaseDate, game.Genre, game.Type, game.Price, comp.Id, plat.Id) };
            }
            else
            {
                return new ApiResult() { OperationResult = false };
            }
        }
    }
}
