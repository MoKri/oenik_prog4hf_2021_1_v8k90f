﻿// <copyright file="PlatformNumsByCompany.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// A Class that is used in a non crud method. It contains the name of a company and a number that shows how many consoles they have.
    /// </summary>
    public class PlatformNumsByCompany
    {
        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets how many consoles the company has.
        /// </summary>
        public int CountNumber { get; set; }

        /// <summary>
        /// A method that returns this object's datas.
        /// </summary>
        /// <returns>A string that contains the company's name and how many consoles it has.</returns>
        public override string ToString()
        {
            return $"[{this.CompanyName}] : {this.CountNumber}";
        }

        /// <summary>
        /// A method that could tell whether the objects are equal or not.
        /// </summary>
        /// <param name="obj">Another PlatformNumsByCompany object that is compared with this object.</param>
        /// <returns>Whether the two of these objects are equal or not.</returns>
        public override bool Equals(object obj)
        {
            PlatformNumsByCompany p = obj as PlatformNumsByCompany;
            return p != null && this.CompanyName == p.CompanyName && this.CountNumber == p.CountNumber;
        }

        /// <summary>
        /// A method that generates a Hash code for this object.
        /// </summary>
        /// <returns>The object's hashcode that the method generated.</returns>
        public override int GetHashCode()
        {
            return (this.CountNumber * 3) - (this.CompanyName.Length / 2);
        }
    }
}
