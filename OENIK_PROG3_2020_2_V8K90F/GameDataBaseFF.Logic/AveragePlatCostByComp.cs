﻿// <copyright file="AveragePlatCostByComp.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// A Class that is used in a non crud method. It contains the name of a company and the averge cost of its released consoles.
    /// </summary>
    public class AveragePlatCostByComp
    {
        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the average cost of the companys consoles.
        /// </summary>
        public double AverageCost { get; set; }

        /// <summary>
        /// A method that returns the object's datas.
        /// </summary>
        /// <returns>A string that contains the company's name and the average cost of its consoles.</returns>
        public override string ToString()
        {
            return $"[{this.CompanyName}] : {this.AverageCost}";
        }

        /// <summary>
        /// A method that could tell whether the objects are equal or not.
        /// </summary>
        /// <param name="obj">Another AveragePlatCostByComp object that is compared with this object.</param>
        /// <returns>Whether the two of these objects are equal or not.</returns>
        public override bool Equals(object obj)
        {
            AveragePlatCostByComp a = obj as AveragePlatCostByComp;
            return a != null && this.CompanyName == a.CompanyName && this.AverageCost == a.AverageCost;
        }

        /// <summary>
        /// A method that generates a Hash code for this object.
        /// </summary>
        /// <returns>The object's hashcode that the method generated.</returns>
        public override int GetHashCode()
        {
            return (int)Math.Floor(this.AverageCost) - this.CompanyName[5].GetHashCode();
        }
    }
}
