﻿// <copyright file="DeviceProduceLogic.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameDataBaseFF.Data;
    using GameDataBaseFF.Repository;

    /// <summary>
    /// Class that shows the relations between Platforms and Companies.
    /// </summary>
    public class DeviceProduceLogic : IDeviceProduceLogic
    {
        private ICompanyRepository companyrepo;

        private IPlatformRepository platformrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DeviceProduceLogic"/> class.
        /// Sets the company repository and the platform repository for this class.
        /// </summary>
        /// <param name="company">The CompanyRepository that will be set.</param>
        /// <param name="platform">The PlatformRepository that will be set.</param>
        public DeviceProduceLogic(ICompanyRepository company, IPlatformRepository platform)
        {
            this.companyrepo = company;
            this.platformrepo = platform;
        }

        /// <summary>
        /// Method that Updates the specified Company's CEO's name in the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the Company is.</param>
        /// <param name="newBoss">The new CEO's name.</param>
        public void ChangeCompanyBoss(int id, string newBoss)
        {
            this.companyrepo.ChangeBoss(id, newBoss);
        }

        /// <summary>
        /// Method that Updates the specified Platform's Storage capacity in the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the Platform is.</param>
        /// <param name="newStorage">The new capacity that will be set for the Platform entity.</param>
        public void ChangePlatformStorage(int id, int newStorage)
        {
            this.platformrepo.ChangeStorage(id, newStorage);
        }

        /// <summary>
        /// Method that creates a platform entity and inserts it into the database.
        /// </summary>
        /// <param name="name">This is what the name of the insertable platform's name is.</param>
        /// <param name="releasedate">This is what the release date of the insertable platform's is.</param>
        /// <param name="price">This is how much the insertable platform costs.</param>
        /// <param name="storage">This is how much capacity the insertable platform has.</param>
        /// <param name="hdmi">This shows whether the insertable platform has an hdmi port or not.</param>
        /// <param name="produceid">This is the id of the platform's maker company.</param>
        public void CreatePlatform(string name, DateTime releasedate, int price, int storage, bool hdmi, int produceid)
        {
            this.platformrepo.Insert(new Platform() { Name = name, ReleaseDate = releasedate, Price = price, Storage = storage, HDMI = hdmi, ProducerId = produceid });
        }

        /// <summary>
        ///  Method that creates a company enttiy and inserts it into the database.
        /// </summary>
        /// <param name="name"> This is the name of the insertable company.</param>
        /// <param name="founddate">This is the founding date of the insertable company.</param>
        /// <param name="hqcity">This is the city where the insertable company's headquarter's is.</param>
        /// <param name="hqcountry">This is the country where the insertable company's headquarter's is.</param>
        /// <param name="ceo">This is the ceo's name of the insertable company.</param>
        public void CreateCompany(string name, DateTime founddate, string hqcity, string hqcountry, string ceo)
        {
            this.companyrepo.Insert(new Company() { Name = name, FoundDate = founddate, HeadquartersCity = hqcity, HeadquartersCountry = hqcountry, CEO = ceo });
        }

        /// <summary>
        /// Method that returns all the Companies from the database.
        /// </summary>
        /// <returns>Company specific IList that contains the Company entities from the database.</returns>
        public IList<Company> GetAllCompanies()
        {
            return this.companyrepo.GetAll().ToList();
        }

        /// <summary>
        /// Method that returns all the platforms from the database.
        /// </summary>
        /// <returns>Platform specific IList that contains the platform entities from the database.</returns>
        public IList<Platform> GetAllPlatforms()
        {
            return this.platformrepo.GetAll().ToList();
        }

        /// <summary>
        /// Method that returns a specific Company from the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the expected Company's is.</param>
        /// <returns>A Company entity.</returns>
        public Company GetOneCompany(int id)
        {
            return this.companyrepo.GetOne(id);
        }

        /// <summary>
        /// Method that returns a specific Platform from the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the expected Platform's is.</param>
        /// <returns>A Platform entity.</returns>
        public Platform GetOnePlatform(int id)
        {
            return this.platformrepo.GetOne(id);
        }

        /// <summary>
        /// Method that Deletes the specified Company from the database.
        /// </summary>
        /// <param name="entity">The removable Company entity.</param>
        /// <returns>Whether the removal was successful or not.</returns>
        public bool RemoveCompany(Company entity)
        {
            return this.companyrepo.Remove(entity);
        }

        /// <summary>
        /// Method that Deletes the specified Platform from the database.
        /// </summary>
        /// <param name="entity">The removable Platform entity.</param>
        /// <returns>Whether the removal was successful or not.</returns>
        public bool RemovePlatform(Platform entity)
        {
            return this.platformrepo.Remove(entity);
        }

        // NON-CRUD

        /// <summary>
        /// Method that returns all the Platforms that does not have an HDMI port.
        /// </summary>
        /// <returns>An IList of Platforms that meets the requirements above.</returns>
        public IList<Platform> NonHDMIPlatforms()
        {
            return this.platformrepo.GetAll().Where(x => !x.HDMI).ToList();
        }

        /// <summary>
        /// Method that is the async version of the NonHDMIPlatforms method.
        /// </summary>
        /// <returns>A Task with a Platform type collection.</returns>
        public Task<IList<Platform>> NonHDMIPlatformsAsync()
        {
            return Task<IList<Platform>>.Factory.StartNew(this.NonHDMIPlatforms);
        }

        /// <summary>
        /// Method that counts how many platforms a company has.
        /// </summary>
        /// <returns>An IList that contains the company names with a number that shows how many platforms they have.</returns>
        public IList<PlatformNumsByCompany> CountPlatformByCompany()
        {
            var cpbc = from platforms in this.platformrepo.GetAll()
                       join companies in this.companyrepo.GetAll() on platforms.ProducerId equals companies.Id
                       group platforms by companies.Name into g
                       orderby g.Count() descending
                       select new PlatformNumsByCompany()
                       {
                           CompanyName = g.Key,
                           CountNumber = g.Count(),
                       };

            return cpbc.ToList();
        }

        /// <summary>
        /// Method that is the async version of the CountPlatformByCompany method.
        /// </summary>
        /// <returns>A Task with a PlatformNumsByCompany type collection.</returns>
        public Task<IList<PlatformNumsByCompany>> CountPlatformByCompanyAsync()
        {
            return Task<IList<PlatformNumsByCompany>>.Factory.StartNew(this.CountPlatformByCompany);
        }

        /// <summary>
        /// Method that averages how much the platforms costs by companies, and orders by the average cost.
        /// </summary>
        /// <returns>An IList that contains the companies with their platforms average cost.</returns>
        public IList<AveragePlatCostByComp> AVGPlatformCostByCompany()
        {
            var avgpcbc = from platforms in this.platformrepo.GetAll()
                          join companies in this.companyrepo.GetAll() on platforms.ProducerId equals companies.Id
                          group platforms by companies.Name into g
                          orderby g.Average(x => x.Price)
                          select new AveragePlatCostByComp()
                          {
                              CompanyName = g.Key,
                              AverageCost = g.Average(x => x.Price),
                          };

            return avgpcbc.ToList();
        }

        /// <summary>
        /// Method that is the async version of the AVGPlatformCostByCompany method.
        /// </summary>
        /// <returns>A Task with a AveragePlatCostByComp type collection.</returns>
        public Task<IList<AveragePlatCostByComp>> AVGPlatformCostByCompanyAsync()
        {
            return Task<IList<AveragePlatCostByComp>>.Factory.StartNew(this.AVGPlatformCostByCompany);
        }
    }
}
