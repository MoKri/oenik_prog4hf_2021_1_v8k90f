﻿// <copyright file="GameByCompany.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// A Class that is used in a non crud method. It contains the name of a company and the name of its released games.
    /// </summary>
    public class GameByCompany
    {
        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets the string that contains the name of the company's games.
        /// </summary>
        public string GameTitles { get; set; }

        /// <summary>
        /// A method that returns this object's datas.
        /// </summary>
        /// <returns>A string that contains the company's name and the company's games.</returns>
        public override string ToString()
        {
            return $"[{this.CompanyName}] : {this.GameTitles}\n";
        }

        /// <summary>
        /// A method that could tell whether the objects are equal or not.
        /// </summary>
        /// <param name="obj">Another GameByCompany object that is compared with this object.</param>
        /// <returns>Whether the two of these objects are equal or not.</returns>
        public override bool Equals(object obj)
        {
            GameByCompany g = obj as GameByCompany;
            return g != null && this.CompanyName == g.CompanyName && this.GameTitles == g.GameTitles;
        }

        /// <summary>
        /// A method that generates a Hash code for this object.
        /// </summary>
        /// <returns>The object's hashcode that the method generated.</returns>
        public override int GetHashCode()
        {
            return this.GameTitles.Length / this.CompanyName.Length;
        }
    }
}
