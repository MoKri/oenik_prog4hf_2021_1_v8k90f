﻿// <copyright file="DevelopmentLogic.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography.X509Certificates;
    using System.Text;
    using System.Threading.Tasks;
    using Castle.Core.Internal;
    using GameDataBaseFF.Data;
    using GameDataBaseFF.Repository;

    /// <summary>
    /// Class that shows the relations between Games and Companies.
    /// </summary>
    public class DevelopmentLogic : IDevelopmentLogic
    {
        private IGameRepository gamerepo;

        private ICompanyRepository companyrepo;

        /// <summary>
        /// Initializes a new instance of the <see cref="DevelopmentLogic"/> class.
        /// Sets the company repository and the platform repository for this class.
        /// </summary>
        /// <param name="game">The GameRepository that will be set.</param>
        /// <param name="company">The CompanyRepository that will be set.</param>
        public DevelopmentLogic(IGameRepository game, ICompanyRepository company)
        {
            this.gamerepo = game;
            this.companyrepo = company;
        }

        /// <inheritdoc/>
        public bool ChangeGame(int id, string name, DateTime release, string genre, string type, int price, int company, int platform)
        {
            var game = this.gamerepo.GetAll().SingleOrDefault(x => x.Id == id);
            if (game == null)
            {
                return false;
            }

            return this.gamerepo.ChangeGame(id, name, release, genre, type, price, company, platform);
        }

        /// <summary>
        /// Method that Updates the specified Company's CEO's name in the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the Company is.</param>
        /// <param name="newBoss">The new CEO's name.</param>
        public void ChangeCompanyBoss(int id, string newBoss)
        {
            this.companyrepo.ChangeBoss(id, newBoss);
        }

        /// <summary>
        /// Method that Updates how much the specified Game costs.
        /// </summary>
        /// <param name="id">This specifies what the ID of the Game is.</param>
        /// <param name="newprice">The new price of the game.</param>
        public void ChangeGamePrice(int id, int newprice)
        {
            this.gamerepo.ChangePrice(id, newprice);
        }

        /// <summary>
        /// Method that creates a game entity and inserts it into the database.
        /// </summary>
        /// <param name="name">This is the name of the insertable game.</param>
        /// <param name="releasedate">This is the release date of the insertable game.</param>
        /// <param name="price">This is how much the insertable game costs.</param>
        /// <param name="genre">This is what the genre of the insertable game is.</param>
        /// <param name="type">This is what the type of the insertable game is.</param>
        /// <param name="companyid">This is the id of the game's developer company.</param>
        /// <param name="platformid">This is the id of the platform, where the game releases.</param>
        public void CreateGame(string name, DateTime releasedate, int price, string genre, string type, int companyid, int platformid)
        {
            this.gamerepo.Insert(new Game() { Name = name, ReleaseDate = releasedate, Price = price, Genre = genre, Type = type, CompanyId = companyid, PlatformId = platformid });
        }

        /// <summary>
        ///  Method that creates a company enttiy and inserts it into the database.
        /// </summary>
        /// <param name="name"> This is the name of the insertable company.</param>
        /// <param name="founddate">This is the founding date of the insertable company.</param>
        /// <param name="hqcity">This is the city where the insertable company's headquarter's is.</param>
        /// <param name="hqcountry">This is the country where the insertable company's headquarter's is.</param>
        /// <param name="ceo">This is the ceo's name of the insertable company.</param>
        public void CreateCompany(string name, DateTime founddate, string hqcity, string hqcountry, string ceo)
        {
            this.companyrepo.Insert(new Company() { Name = name, FoundDate = founddate, HeadquartersCity = hqcity, HeadquartersCountry = hqcountry, CEO = ceo });
        }

        /// <summary>
        /// Method that returns all the Companies from the database.
        /// </summary>
        /// <returns>Company specific IList that contains the Company entities from the database.</returns>
        public IList<Company> GetAllCompanies()
        {
            return this.companyrepo.GetAll().ToList();
        }

        /// <summary>
        /// Method that returns all the Games from the database.
        /// </summary>
        /// <returns>Game specific IList that contains the games entities from the database.</returns>
        public IList<Game> GetAllGames()
        {
            return this.gamerepo.GetAll().ToList();
        }

        /// <summary>
        /// Method that returns a specific Company from the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the expected Company's is.</param>
        /// <returns>A Company entity.</returns>
        public Company GetOneCompany(int id)
        {
            return this.companyrepo.GetOne(id);
        }

        /// <summary>
        /// Method that returns a specific Game from the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the expected Game's is.</param>
        /// <returns>A Game entity.</returns>
        public Game GetOneGame(int id)
        {
            return this.gamerepo.GetOne(id);
        }

        /// <summary>
        /// Method that Deletes the specified Company from the database.
        /// </summary>
        /// <param name="entity">The removable Company entity.</param>
        /// <returns>Whether the removal was successful or not.</returns>
        public bool RemoveCompany(Company entity)
        {
            return this.companyrepo.Remove(entity);
        }

        /// <summary>
        /// Method that Deletes the specified Game from the database.
        /// </summary>
        /// <param name="entity">The removable Game entity.</param>
        /// <returns>Whether the removal was successful or not.</returns>
        public bool RemoveGame(Game entity)
        {
            return this.gamerepo.Remove(entity);
        }

        // NON-CRUD

        /// <summary>
        /// Method that returns what the average price of the games are in the database.
        /// </summary>
        /// <returns>The average price of the games.</returns>
        public double AverageGamePrice()
        {
            return this.gamerepo.GetAll().Average(x => x.Price);
        }

        /// <summary>
        /// Method that returns the Game entities grouped by their developer companies.
        /// </summary>
        /// <returns>An IList that the companies name with their developed games names.</returns>
        public IList<GameByCompany> GamesByCompanies()
        {
            var gbyc = from companies in this.companyrepo.GetAll()
                       select new GameByCompany()
                       {
                           CompanyName = companies.Name,
                           GameTitles = string.Join(',', companies.Games.Select(x => x.Name)),
                       };

            return gbyc.ToList();
        }

        /// <summary>
        /// Method that is the async version of the GamesByCompanies method.
        /// </summary>
        /// <returns>A Task with a GameByCompany type collection.</returns>
        public Task<IList<GameByCompany>> GamesByCompaniesAsync()
        {
            return Task<IList<GameByCompany>>.Factory.StartNew(this.GamesByCompanies);
        }

        /// <summary>
        /// Method that counts how much the games costs by companies.
        /// </summary>
        /// <returns>An IList that contains companies with the cost of their games combined.</returns>
        public IList<GamePriceByCompany> SumGamesPriceByCompany()
        {
            var sgpbc = from games in this.gamerepo.GetAll()
                        join companies in this.companyrepo.GetAll() on games.CompanyId equals companies.Id
                        group games by companies.Name into g
                        select new GamePriceByCompany()
                        {
                            CompanyName = g.Key,
                            SumPrice = g.Sum(x => x.Price),
                        };

            return sgpbc.ToList();
        }

        /// <summary>
        /// Method that is the async version of the SumGamesPriceByCompany method.
        /// </summary>
        /// <returns>A Task with a GamePriceByCompany type collection.</returns>
        public Task<IList<GamePriceByCompany>> SumGamesPriceByCompanyAsync()
        {
            return Task<IList<GamePriceByCompany>>.Factory.StartNew(this.SumGamesPriceByCompany);
        }
    }
}
