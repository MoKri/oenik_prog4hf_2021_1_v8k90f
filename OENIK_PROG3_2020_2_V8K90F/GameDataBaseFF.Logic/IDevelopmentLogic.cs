﻿// <copyright file="IDevelopmentLogic.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GameDataBaseFF.Data;
    using GameDataBaseFF.Repository;

    /// <summary>
    ///  Interface that contains the method definitions for the CRUD methods. It is used for the relations between Games and Companies.
    /// </summary>
    public interface IDevelopmentLogic
    {
        /// <summary>
        /// Changes an entire game.
        /// </summary>
        /// <param name="id">the id of the game.</param>
        /// <param name="name">the name of the game.</param>
        /// <param name="release">the release of the game.</param>
        /// <param name="genre">the genre of the game.</param>
        /// <param name="type">the type of the game.</param>
        /// <param name="price">the price of the game.</param>
        /// <param name="company">the company of the game.</param>
        /// <param name="platform">the platform of the game.</param>
        /// <returns>Whether the game changed or not.</returns>
        bool ChangeGame(int id, string name, DateTime release, string genre, string type, int price, int company, int platform);

        /// <summary>
        /// Method that returns all the Games from the database.
        /// </summary>
        /// <returns>Game specific IList that contains the games entities from the database.</returns>
        IList<Game> GetAllGames();

        /// <summary>
        /// Method that returns all the Companies from the database.
        /// </summary>
        /// <returns>Company specific IList that contains the Company entities from the database.</returns>
        IList<Company> GetAllCompanies();

        /// <summary>
        /// Method that returns a specific Game from the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the expected Game's is.</param>
        /// <returns>A Game entity.</returns>
        Game GetOneGame(int id);

        /// <summary>
        /// Method that returns a specific Company from the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the expected Company's is.</param>
        /// <returns>A Company entity.</returns>
        Company GetOneCompany(int id);

        /// <summary>
        /// Method that creates a game entity and inserts it into the database.
        /// </summary>
        /// <param name="name">This is the name of the insertable game.</param>
        /// <param name="releasedate">This is the release date of the insertable game.</param>
        /// <param name="price">This is how much the insertable game costs.</param>
        /// <param name="genre">This is what the genre of the insertable game is.</param>
        /// <param name="type">This is what the type of the insertable game is.</param>
        /// <param name="companyid">This is the id of the game's developer company.</param>
        /// <param name="platformid">This is the id of the platform, where the game releases.</param>
        void CreateGame(string name, DateTime releasedate, int price, string genre, string type, int companyid, int platformid);

        /// <summary>
        ///  Method that creates a company enttiy and inserts it into the database.
        /// </summary>
        /// <param name="name"> This is the name of the insertable company.</param>
        /// <param name="founddate">This is the founding date of the insertable company.</param>
        /// <param name="hqcity">This is the city where the insertable company's headquarter's is.</param>
        /// <param name="hqcountry">This is the country where the insertable company's headquarter's is.</param>
        /// <param name="ceo">This is the ceo's name of the insertable company.</param>
        void CreateCompany(string name, DateTime founddate, string hqcity, string hqcountry, string ceo);

        /// <summary>
        /// Method that Updates how much the specified Game costs.
        /// </summary>
        /// <param name="id">This specifies what the ID of the Game is.</param>
        /// <param name="newprice">The new price of the game.</param>
        void ChangeGamePrice(int id, int newprice);

        /// <summary>
        /// Method that Updates the specified Company's CEO's name in the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the Company is.</param>
        /// <param name="newBoss">The new CEO's name.</param>
        void ChangeCompanyBoss(int id, string newBoss);

        /// <summary>
        /// Method that Deletes the specified Game from the database.
        /// </summary>
        /// <param name="entity">The removable Game entity.</param>
        /// <returns>Whether the removal was successful or not.</returns>
        bool RemoveGame(Game entity);

        /// <summary>
        /// Method that Deletes the specified Company from the database.
        /// </summary>
        /// <param name="entity">The removable Company entity.</param>
        /// <returns>Whether the removal was successful or not.</returns>
        bool RemoveCompany(Company entity);
    }
}
