﻿// <copyright file="IDeviceProduceLogic.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GameDataBaseFF.Data;

    /// <summary>
    /// Interface that contains the method definitions for the CRUD methods. It is used for the relations between Platforms and Companies.
    /// </summary>
    public interface IDeviceProduceLogic
    {
        /// <summary>
        /// Method that returns all the platforms from the database.
        /// </summary>
        /// <returns>Platform specific IList that contains the platform entities from the database.</returns>
        IList<Platform> GetAllPlatforms();

        /// <summary>
        /// Method that returns all the Companies from the database.
        /// </summary>
        /// <returns>Company specific IList that contains the Company entities from the database.</returns>
        IList<Company> GetAllCompanies();

        /// <summary>
        /// Method that returns a specific Platform from the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the expected Platform's is.</param>
        /// <returns>A Platform entity.</returns>
        Platform GetOnePlatform(int id);

        /// <summary>
        /// Method that returns a specific Company from the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the expected Company's is.</param>
        /// <returns>A Company entity.</returns>
        Company GetOneCompany(int id);

        /// <summary>
        /// Method that creates a platform entity and inserts it into the database.
        /// </summary>
        /// <param name="name">This is what the name of the insertable platform's name is.</param>
        /// <param name="releasedate">This is what the release date of the insertable platform's is.</param>
        /// <param name="price">This is how much the insertable platform costs.</param>
        /// <param name="storage">This is how much capacity the insertable platform has.</param>
        /// <param name="hdmi">This shows whether the insertable platform has an hdmi port or not.</param>
        /// <param name="produceid">This is the id of the platform's maker company.</param>
        void CreatePlatform(string name, DateTime releasedate, int price, int storage, bool hdmi, int produceid);

        /// <summary>
        ///  Method that creates a company enttiy and inserts it into the database.
        /// </summary>
        /// <param name="name"> This is the name of the insertable company.</param>
        /// <param name="founddate">This is the founding date of the insertable company.</param>
        /// <param name="hqcity">This is the city where the insertable company's headquarter's is.</param>
        /// <param name="hqcountry">This is the country where the insertable company's headquarter's is.</param>
        /// <param name="ceo">This is the ceo's name of the insertable company.</param>
        void CreateCompany(string name, DateTime founddate, string hqcity, string hqcountry, string ceo);

        /// <summary>
        /// Method that Updates the specified Platform's Storage capacity in the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the Platform is.</param>
        /// <param name="newStorage">The new capacity that will be set for the Platform entity.</param>
        void ChangePlatformStorage(int id, int newStorage);

        /// <summary>
        /// Method that Updates the specified Company's CEO's name in the database.
        /// </summary>
        /// <param name="id">This specifies what the ID of the Company is.</param>
        /// <param name="newBoss">The new CEO's name.</param>
        void ChangeCompanyBoss(int id, string newBoss);

        /// <summary>
        /// Method that Deletes the specified Platform from the database.
        /// </summary>
        /// <param name="entity">The removable Platform entity.</param>
        /// <returns>Whether the removal was successful or not.</returns>
        bool RemovePlatform(Platform entity);

        /// <summary>
        /// Method that Deletes the specified Company from the database.
        /// </summary>
        /// <param name="entity">The removable Company entity.</param>
        /// <returns>Whether the removal was successful or not.</returns>
        bool RemoveCompany(Company entity);
    }
}
