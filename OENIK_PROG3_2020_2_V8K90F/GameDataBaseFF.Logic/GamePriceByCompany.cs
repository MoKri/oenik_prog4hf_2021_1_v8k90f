﻿// <copyright file="GamePriceByCompany.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// A Class that is used in a non crud method. It contains the name of a company and a number that shows how much the company's games are worth.
    /// </summary>
    public class GamePriceByCompany
    {
        /// <summary>
        /// Gets or sets the name of the company.
        /// </summary>
        public string CompanyName { get; set; }

        /// <summary>
        /// Gets or sets how much the company's games are worth.
        /// </summary>
        public int SumPrice { get; set; }

        /// <summary>
        /// A method that returns this object's datas.
        /// </summary>
        /// <returns>A string that contains the company's name and how much its games are worth.</returns>
        public override string ToString()
        {
            return $"{this.CompanyName}\t{this.SumPrice}";
        }

        /// <summary>
        /// A method that could tell whether the objects are equal or not.
        /// </summary>
        /// <param name="obj">Another GamePriceByCompany object that is compared with this object.</param>
        /// <returns>Whether the two of these objects are equal or not.</returns>
        public override bool Equals(object obj)
        {
            GamePriceByCompany g = obj as GamePriceByCompany;
            return g != null && this.CompanyName == g.CompanyName && this.SumPrice == g.SumPrice;
        }

        /// <summary>
        /// A method that generates a Hash code for this object.
        /// </summary>
        /// <returns>The object's hashcode that the method generated.</returns>
        public override int GetHashCode()
        {
            return this.CompanyName.Length * (this.SumPrice / 50);
        }
    }
}
