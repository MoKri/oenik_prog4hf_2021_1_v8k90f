﻿// <copyright file="GameVM.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.WpfClient.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Game ViewModel.
    /// </summary>
    public class GameVM : ObservableObject
    {
        private int id;
        private string name;
        private DateTime release;
        private string genre;
        private string type;
        private int price;
        private string companyname;
        private string platformname;

        /// <summary>
        /// Gets or sets the games id.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets the games name.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the games releasedate.
        /// </summary>
        public DateTime Release
        {
            get { return this.release; }
            set { this.Set(ref this.release, value); }
        }

        /// <summary>
        /// Gets or sets the games genre.
        /// </summary>
        public string Genre
        {
            get { return this.genre; }
            set { this.Set(ref this.genre, value); }
        }

        /// <summary>
        /// Gets or sets the games type.
        /// </summary>
        public string Type
        {
            get { return this.type; }
            set { this.Set(ref this.type, value); }
        }

        /// <summary>
        /// Gets or sets the games price.
        /// </summary>
        public int Price
        {
            get { return this.price; }
            set { this.Set(ref this.price, value); }
        }

        /// <summary>
        /// Gets or sets the games company name.
        /// </summary>
        public string CompanyName
        {
            get { return this.companyname; }
            set { this.Set(ref this.companyname, value); }
        }

        /// <summary>
        /// Gets or sets the games platform name.
        /// </summary>
        public string PlatformName
        {
            get { return this.platformname; }
            set { this.Set(ref this.platformname, value); }
        }

        /// <summary>
        /// Copies another games details to this.
        /// </summary>
        /// <param name="other">The other game.</param>
        public void CopyFrom(GameVM other)
        {
            if (other == null)
            {
                return;
            }
            else
            {
                this.Id = other.Id;
                this.Name = other.Name;
                this.Release = other.Release;
                this.Price = other.Price;
                this.Genre = other.Genre;
                this.Type = other.Type;
                this.CompanyName = other.CompanyName;
                this.PlatformName = other.PlatformName;
            }
        }
    }
}
