﻿// <copyright file="MainVM.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.WpfClient.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GameDataBaseFF.WpfClient.BL;

    /// <summary>
    /// Main ViewModel.
    /// </summary>
    public class MainVM : ViewModelBase
    {
        private IMainLogic logic;
        private GameVM selectedgame;
        private ObservableCollection<GameVM> allGames;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        /// <param name="logic">The logic which will use the database.</param>
        public MainVM(IMainLogic logic)
        {
            this.logic = logic;

            this.LoadCmd = new RelayCommand(() => this.AllGames = new ObservableCollection<GameVM>(this.logic.ApiGetGames()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelGame(this.selectedgame));
            this.AddCmd = new RelayCommand(() => this.logic.EditGame(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditGame(this.selectedgame, this.EditorFunc));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IMainLogic>())
        {
        }

        /// <summary>
        /// Gets or sets the game collection.
        /// </summary>
        public ObservableCollection<GameVM> AllGames
        {
            get
            {
                return this.allGames;
            }

            set
            {
                this.Set(ref this.allGames, value);
            }
        }

        /// <summary>
        /// Gets or sets the selected game.
        /// </summary>
        public GameVM SelectedGame
        {
            get
            {
                return this.selectedgame;
            }

            set
            {
                this.Set(ref this.selectedgame, value);
            }
        }

        /// <summary>
        /// Gets or sets the editor function.
        /// </summary>
        public Func<GameVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets the adder command.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets the delete command.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets the modify command.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets the loader command.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
