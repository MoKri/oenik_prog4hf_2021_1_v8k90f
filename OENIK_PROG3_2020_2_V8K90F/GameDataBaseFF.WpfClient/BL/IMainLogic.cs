﻿// <copyright file="IMainLogic.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.WpfClient.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameDataBaseFF.WpfClient.VM;

    /// <summary>
    /// Interface for mainlogic.
    /// </summary>
    public interface IMainLogic
    {
        /// <summary>
        /// Gets games.
        /// </summary>
        /// <returns>Games.</returns>
        IList<GameVM> ApiGetGames();

        /// <summary>
        /// Deletes games.
        /// </summary>
        /// <param name="game">game which will be deleted.</param>
        void ApiDelGame(GameVM game);

        /// <summary>
        /// Edits games.
        /// </summary>
        /// <param name="game">game we want to edit.</param>
        /// <param name="editorFunc">how we want to edit.</param>
        void EditGame(GameVM game, Func<GameVM, bool> editorFunc);
    }
}
