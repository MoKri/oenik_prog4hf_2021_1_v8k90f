﻿// <copyright file="MainLogic.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.WpfClient.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using GameDataBaseFF.WpfClient.VM;

    /// <summary>
    /// MainLogic.
    /// </summary>
    public class MainLogic : IMainLogic, IDisposable
    {
        private string url = "http://localhost:36415/GamesApi/";
        private HttpClient client = new HttpClient();
        private JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <returns>list of games.</returns>
        public IList<GameVM> ApiGetGames()
        {
            string json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<GameVM>>(json, this.jsonOptions);

            var datehelper = this.client.GetStringAsync(this.url + "all").Result.Split('{').ToList();
            List<string> dates = new List<string>();
            for (int i = 1; i < datehelper.Count; i++)
            {
                dates.Add(datehelper[i].Split('\"')[9]);
            }

            for (int i = 0; i < dates.Count; i++)
            {
                dates[i] = dates[i].Substring(0, 10);
            }

            List<DateTime> dataDates = new List<DateTime>();

            foreach (var item in dates)
            {
                dataDates.Add(new DateTime(int.Parse(item.Split('-')[0]), int.Parse(item.Split('-')[1]), int.Parse(item.Split('-')[2])));
            }

            for (int i = 0; i < list.Count; i++)
            {
                list[i].Release = dataDates[i];
            }

            return list;
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="game">game.</param>
        public void ApiDelGame(GameVM game)
        {
            bool success = false;

            if (game != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + game.Id.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <summary>
        /// <inheritdoc />
        /// </summary>
        /// <param name="game">game.</param>
        /// <param name="editorFunc">editor.</param>
        public void EditGame(GameVM game, Func<GameVM, bool> editorFunc)
        {
            GameVM clone = new GameVM();
            if (game != null)
            {
                clone.CopyFrom(game);
            }

            bool success = false;

            if (editorFunc != null)
            {
                success = editorFunc.Invoke(clone);
            }

            if (success)
            {
                if (game != null)
                {
                    success = this.ApiEditGame(clone, true);
                }
                else
                {
                    success = this.ApiEditGame(clone, false);
                }
            }

            this.SendMessage(success == true);
        }

        /// <summary>
        /// Disposes HttpClient.
        /// </summary>
        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes HttpClient.
        /// </summary>
        /// <param name="disposing">Whether dispose needs to happen.</param>
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.client.Dispose();
            }
        }

        private bool ApiEditGame(GameVM game, bool isEditing)
        {
            if (game == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");

            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("id", game.Id.ToString());
            }

            string date = game.Release.Year.ToString() + "-" + game.Release.Month.ToString().PadLeft(2, '0') + "-" + game.Release.Day.ToString().PadLeft(2, '0') + "T00:00:00";
            postData.Add("name", game.Name.ToString());
            postData.Add("releasedate", date);
            postData.Add("genre", game.Genre);
            postData.Add("type", game.Type);
            postData.Add("price", game.Price.ToString());
            postData.Add("companyname", game.CompanyName);
            postData.Add("platformname", game.PlatformName);

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }

        private void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully!" : "Operation failed!";
            Messenger.Default.Send(msg, "GameResult");
        }
    }
}
