﻿// <copyright file="GlobalSuppressions.cs" company="V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

// This file is used by Code Analysis to maintain SuppressMessage
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given
// a specific target and scoped to a namespace, type, member, etc.
using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I had to use int.prase.", Scope = "member", Target = "~M:GameDataBaseFF.WpfClient.UI.DateToStringConverter.ConvertBack(System.Object,System.Type,System.Object,System.Globalization.CultureInfo)~System.Object")]
[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I had to use int.prase.", Scope = "member", Target = "~M:GameDataBaseFF.WpfClient.BL.MainLogic.ApiGetGames~System.Collections.Generic.IList{GameDataBaseFF.WpfClient.VM.GameVM}")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "I have to use that.", Scope = "member", Target = "~M:GameDataBaseFF.WpfClient.BL.MainLogic.ApiGetGames~System.Collections.Generic.IList{GameDataBaseFF.WpfClient.VM.GameVM}")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "I have to use that.", Scope = "member", Target = "~M:GameDataBaseFF.WpfClient.BL.MainLogic.ApiDelGame(GameDataBaseFF.WpfClient.VM.GameVM)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I have to use that.", Scope = "member", Target = "~M:GameDataBaseFF.WpfClient.BL.MainLogic.ApiDelGame(GameDataBaseFF.WpfClient.VM.GameVM)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I have to use that.", Scope = "member", Target = "~M:GameDataBaseFF.WpfClient.BL.MainLogic.ApiEditGame(GameDataBaseFF.WpfClient.VM.GameVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Usage", "CA2234:Pass system uri objects instead of strings", Justification = "I have to use that.", Scope = "member", Target = "~M:GameDataBaseFF.WpfClient.BL.MainLogic.ApiEditGame(GameDataBaseFF.WpfClient.VM.GameVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Reliability", "CA2000:Dispose objects before losing scope", Justification = "I have to use that.", Scope = "member", Target = "~M:GameDataBaseFF.WpfClient.BL.MainLogic.ApiEditGame(GameDataBaseFF.WpfClient.VM.GameVM,System.Boolean)~System.Boolean")]
[assembly: SuppressMessage("Performance", "CA1822:Mark members as static", Justification = "I have to use it so it cant be static.", Scope = "member", Target = "~M:GameDataBaseFF.WpfClient.BL.MainLogic.SendMessage(System.Boolean)")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "I don't want it to be read-only.", Scope = "member", Target = "~P:GameDataBaseFF.WpfClient.VM.MainVM.AllGames")]
