﻿// <copyright file="IEditorService.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameDataBaseFF.Data;

    /// <summary>
    /// Interface for editing.
    /// </summary>
    public interface IEditorService
    {
        /// <summary>
        /// Method to edit games.
        /// </summary>
        /// <param name="g">The game that we want to edit.</param>
        /// <returns>Whether the edit was succesful or not.</returns>
        bool EditGame(Game g);
    }
}
