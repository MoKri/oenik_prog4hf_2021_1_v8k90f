﻿// <copyright file="GameLogic.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using GameDataBaseFF.Data;
    using GameDataBaseFF.Logic;

    /// <summary>
    /// The GameLogic class that is used in the WPF.
    /// </summary>
    public class GameLogic : IGameLogic
    {
        private readonly IEditorService editorService;
        private readonly IMessenger messengerService;
        private readonly IDevelopmentLogic devLogic;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameLogic"/> class.
        /// </summary>
        /// <param name="editorService">Will be implementing the editable methods.</param>
        /// <param name="messengerService">Will send the notifications about what happened.</param>
        /// <param name="devLogic">The logic that will use the database methods.</param>
        public GameLogic(IEditorService editorService, IMessenger messengerService, IDevelopmentLogic devLogic)
        {
            this.editorService = editorService;
            this.messengerService = messengerService;
            this.devLogic = devLogic;
        }

        // public IList<Company> AllCompanies { get { return this.devLogic.GetAllCompanies(); } }

        /// <summary>
        /// Method that adds a game into a list.
        /// </summary>
        /// <param name="list">The list where we want the game to be added.</param>
        public void AddGame(IList<Game> list)
        {
            Game newGame = new Game() { CompanyId = 1, PlatformId = 1 };

            if (this.editorService.EditGame(newGame))
            {
                this.devLogic.CreateGame(newGame.Name, newGame.ReleaseDate, newGame.Price, newGame.Genre, newGame.Type, newGame.CompanyId, newGame.PlatformId);
                list?.Add(newGame);

                this.messengerService.Send("Inserting Succesful!", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Inserting Failed.", "LogicResult");
            }
        }

        /// <summary>
        /// Method that deletes a game.
        /// </summary>
        /// <param name="list">The list where we want the game to be deleted from.</param>
        /// <param name="g">The game that we want to be deleted.</param>
        public void DeleteGame(IList<Game> list, Game g)
        {
            if (g != null && list != null && list.Remove(g))
            {
                this.devLogic.RemoveGame(g);
                this.messengerService.Send("Delete Succesful", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Delete Failed!", "LogicResult");
            }
        }

        /// <summary>
        /// Method that returns all of the games from the database.
        /// </summary>
        /// <returns>A list that contains games.</returns>
        public IList<Game> GetAllGames()
        {
            return this.devLogic.GetAllGames();
        }

        /// <summary>
        /// Method that mopdifies a game.
        /// </summary>
        /// <param name="g">The game that we want to modify.</param>
        public void ModifyGame(Game g)
        {
            if (g == null)
            {
                this.messengerService.Send("Modify Failed.", "LogicResult");
                return;
            }

            Game clone = new Game();
            clone.CopyFrom(g);
            if (this.editorService.EditGame(clone))
            {
                g.CopyFrom(clone);
                this.messengerService.Send("Modify Succesful!", "LogicResult");
            }
            else
            {
                this.messengerService.Send("Modify Failed.", "LogicResult");
            }
        }
    }
}
