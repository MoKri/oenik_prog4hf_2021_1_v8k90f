﻿// <copyright file="IGameLogic.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameDataBaseFF.Data;

    /// <summary>
    /// Interface for the Game logic.
    /// </summary>
    public interface IGameLogic
    {
        /// <summary>
        /// Method that adds a game into a list.
        /// </summary>
        /// <param name="list">The list where we want the game to be added.</param>
        void AddGame(IList<Game> list);

        /// <summary>
        /// Method that mopdifies a game.
        /// </summary>
        /// <param name="g">The game that we want to modify.</param>
        void ModifyGame(Game g);

        /// <summary>
        /// Method that deletes a game.
        /// </summary>
        /// <param name="list">The list where we want the game to be deleted from.</param>
        /// <param name="g">The game that we want to be deleted.</param>
        void DeleteGame(IList<Game> list, Game g);

        /// <summary>
        /// Method that returns all of the games from the database.
        /// </summary>
        /// <returns>A list that contains games.</returns>
        IList<Game> GetAllGames();
    }
}
