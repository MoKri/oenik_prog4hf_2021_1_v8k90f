﻿// <copyright file="EditorViewModel.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using GameDataBaseFF.Data;

    /// <summary>
    /// ViewModel class that will be used for the editor window.
    /// </summary>
    public class EditorViewModel : ViewModelBase
    {
        private Game game;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.game = new Game();

            if (this.IsInDesignMode)
            {
                this.game.Name = "Teszt game";
                this.game.Price = 18000;
                this.game.Type = "valami";
            }
        }

        /// <summary>
        /// Gets the ids for the companies.
        /// </summary>
        public static IList<int> Companies
        {
            get
            {
                return Enumerable.Range(1, 9).ToList();
            }
        }

        /// <summary>
        /// Gets the ids for the platforms.
        /// </summary>
        public static IList<int> Platforms
        {
            get
            {
                return Enumerable.Range(1, 6).ToList();
            }
        }

        /// <summary>
        /// Gets or sets the game that will be edited.
        /// </summary>
        public Game Game { get => this.game; set => this.Set(ref this.game, value); }
    }
}
