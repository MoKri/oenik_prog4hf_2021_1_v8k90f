﻿// <copyright file="MainViewModel.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using GameDataBaseFF.Data;
    using GameWPF.BL;

    /// <summary>
    /// ViewModel class that is used on the main window.
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        private readonly IGameLogic gameLogic;

        private Game selectedGame;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="gl">The logic that will be used to alter the database.</param>
        public MainViewModel(IGameLogic gl)
        {
            this.gameLogic = gl;
            this.Games = new ObservableCollection<Game>();
            this.SelectedGame = new Game();

            if (this.IsInDesignMode)
            {
                this.Games.Add(new Game() { Id = 1, Name = "Cyberpunk 2077", ReleaseDate = new DateTime(2020, 11, 19), Price = 19000, Genre = "cyberpunk/shooter", Type = "FPS" });
                this.Games.Add(new Game() { Id = 2, Name = "Red Dead Redemption 2", ReleaseDate = new DateTime(2018, 10, 26), Price = 12000, Genre = "open-world/shooter/wild-west", Type = "TPS" });
                this.Games.Add(new Game() { Id = 3, Name = "God of War (2018)", ReleaseDate = new DateTime(2018, 04, 20), Price = 6500, Genre = "hack-n-slash/mythology", Type = "TPS" });
                this.Games.Add(new Game() { Id = 4, Name = "Assassin's Creed Valhalla", ReleaseDate = new DateTime(2020, 11, 10), Price = 21000, Genre = "open-world/history", Type = "TPS" });
                this.selectedGame = this.Games[2];
            }
            else
            {
                this.gameLogic.GetAllGames().ToList().ForEach(g => this.Games.Add(g));
            }

            this.AddCommand = new RelayCommand(() => { this.gameLogic.AddGame(this.Games); });
            this.ModifyCommand = new RelayCommand(() => { this.gameLogic.ModifyGame(this.selectedGame); });
            this.DelCommand = new RelayCommand(() => { this.gameLogic.DeleteGame(this.Games, this.SelectedGame); });
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IGameLogic>())
        {
        }

        /// <summary>
        /// Gets all of the games.
        /// </summary>
        public ObservableCollection<Game> Games { get; private set; }

        /// <summary>
        /// Gets or sets the selected game from the main window.
        /// </summary>
        public Game SelectedGame { get => this.selectedGame; set => this.Set(ref this.selectedGame, value); }

        /// <summary>
        /// Gets the command that will add a new game.
        /// </summary>
        public ICommand AddCommand { get; private set; }

        /// <summary>
        /// Gets the command that will delete a game.
        /// </summary>
        public ICommand DelCommand { get; private set; }

        /// <summary>
        /// Gets the command that will modify a game.
        /// </summary>
        public ICommand ModifyCommand { get; private set; }
    }
}
