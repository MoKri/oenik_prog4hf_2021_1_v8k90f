﻿// <copyright file="App.xaml.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Data;
    using System.Linq;
    using System.Threading.Tasks;
    using System.Windows;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;
    using GalaSoft.MvvmLight.Messaging;
    using GameDataBaseFF.Data.Models;
    using GameDataBaseFF.Logic;
    using GameDataBaseFF.Repository;
    using GameWPF.BL;
    using GameWPF.UI;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Interaction logic for App.xaml.
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        public App()
        {
            ServiceLocator.SetLocatorProvider(() => MyIoc.Instance);

            MyIoc.Instance.Register<IGameLogic, GameLogic>();

            MyIoc.Instance.Register<IEditorService, EditorViaWindow>();

            MyIoc.Instance.Register<IGameRepository, GameRepository>();

            MyIoc.Instance.Register<ICompanyRepository, CompanyRepository>();

            MyIoc.Instance.Register<IPlatformRepository, PlatformRepository>();

            MyIoc.Instance.Register<IDevelopmentLogic, DevelopmentLogic>();

            MyIoc.Instance.Register<IDeviceProduceLogic, DeviceProduceLogic>();

            MyIoc.Instance.Register<IMessenger>(() => Messenger.Default);

            MyIoc.Instance.Register<DbContext, GamesContext>();
        }
    }
}
