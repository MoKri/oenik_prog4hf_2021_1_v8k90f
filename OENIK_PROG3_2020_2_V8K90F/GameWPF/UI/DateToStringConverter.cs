﻿// <copyright file="DateToStringConverter.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Converter class that converts dates into strings and strings into dates.
    /// </summary>
    public class DateToStringConverter : IValueConverter
    {
        /// <summary>
        /// Converts dates into string.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>A string.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime time = (DateTime)value;
            return $"{time.Year}/{time.Month}/{time.Day}";
        }

        /// <summary>
        /// Converts string into date.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>A date.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string stime = value.ToString();
            DateTime newTime = new DateTime(int.Parse(stime.Split('/')[0]), int.Parse(stime.Split('/')[1]), int.Parse(stime.Split('/')[2]));
            return newTime;
        }
    }
}
