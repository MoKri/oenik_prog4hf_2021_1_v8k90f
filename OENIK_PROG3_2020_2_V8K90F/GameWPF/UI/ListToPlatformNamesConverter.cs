﻿// <copyright file="ListToPlatformNamesConverter.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Converter class that will return platform names or their ids.
    /// </summary>
    public class ListToPlatformNamesConverter : IValueConverter
    {
        /// <summary>
        /// Returns a list of the platform names.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>A string list.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            List<string> vissza = new List<string>();

            vissza.Add("Ps4");
            vissza.Add("Ps5");
            vissza.Add("Xbox One");
            vissza.Add("Xbox Series X");
            vissza.Add("Gameboy Color");

            return vissza;
        }

        /// <summary>
        /// Returns a list of the platform ids.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>An int list.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Enumerable.Range(0, 6).ToList();
        }
    }
}
