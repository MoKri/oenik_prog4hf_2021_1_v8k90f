﻿// <copyright file="IntToPlatformNamesConverter.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Converter class that will return platform names to their ids or platform ids to their names.
    /// </summary>
    public class IntToPlatformNamesConverter : IValueConverter
    {
        /// <summary>
        /// Converts ints into platform names as string.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>A platform name as string.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int num = (int)value;
            switch (num)
            {
                default:
                case 1: return "Ps4";
                case 2: return "Ps5";
                case 3: return "Xbox One";
                case 4: return "Xbox Series X";
                case 5: return "Gameboy Color";
            }
        }

        /// <summary>
        /// Converts platform names into ints.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>An int.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string val = (string)value;

            switch (val)
            {
                default:
                case "Ps4": return 1;
                case "Ps5":
                    return 2;
                case "Xbox One":
                    return 3;
                case "Xbox Series X":
                    return 4;
                case "Gameboy Color":
                    return 5;
            }
        }
    }
}
