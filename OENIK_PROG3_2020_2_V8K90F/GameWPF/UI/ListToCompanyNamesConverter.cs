﻿// <copyright file="ListToCompanyNamesConverter.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Converter class that will return company names or their ids.
    /// </summary>
    public class ListToCompanyNamesConverter : IValueConverter
    {
        /// <summary>
        /// Converts a list into a list with names.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>A string list.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            List<string> vissza = new List<string>();

            vissza.Add("CD Projekt Red");
            vissza.Add("Rockstar Games");
            vissza.Add("Ubisoft Monrtreal");
            vissza.Add("Santa Monica Studios");
            vissza.Add("Rare");
            vissza.Add("Nintendo");
            vissza.Add("Sony");
            vissza.Add("Microsoft");

            return vissza;
        }

        /// <summary>
        /// Returns the ids of the companies.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>An int list.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Enumerable.Range(0, 9).ToList();
        }
    }
}
