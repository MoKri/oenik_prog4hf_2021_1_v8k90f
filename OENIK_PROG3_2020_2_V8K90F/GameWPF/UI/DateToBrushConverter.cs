﻿// <copyright file="DateToBrushConverter.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;
    using System.Windows.Media;

    /// <summary>
    /// Converter class that will convert dates into colors.
    /// </summary>
    public class DateToBrushConverter : IValueConverter
    {
        /// <summary>
        /// Converts dates into colors.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>A color.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            DateTime time = (DateTime)value;

            if (time.Year < 2000)
            {
                return Brushes.SandyBrown;
            }
            else if (time.Year < 2015)
            {
                return Brushes.LightBlue;
            }
            else
            {
                return Brushes.Azure;
            }
        }

        /// <summary>
        /// Doesn't do anything.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>Nothing.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return Binding.DoNothing;
        }
    }
}
