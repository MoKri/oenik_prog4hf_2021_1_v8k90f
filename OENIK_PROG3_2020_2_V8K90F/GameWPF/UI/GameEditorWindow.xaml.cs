﻿// <copyright file="GameEditorWindow.xaml.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using GameDataBaseFF.Data;
    using GameWPF.VM;

    /// <summary>
    /// Interaction logic for GameEditorWindow.xaml.
    /// </summary>
    public partial class GameEditorWindow : Window
    {
        private readonly EditorViewModel evm;

        /// <summary>
        /// Initializes a new instance of the <see cref="GameEditorWindow"/> class.
        /// </summary>
        public GameEditorWindow()
        {
            this.InitializeComponent();
            this.evm = this.FindResource("VM") as EditorViewModel;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="GameEditorWindow"/> class.
        /// </summary>
        /// <param name="g">The game that will be edited.</param>
        public GameEditorWindow(Game g)
            : this()
        {
            this.evm.Game = g;
        }

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = true;
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.DialogResult = false;
        }
    }
}
