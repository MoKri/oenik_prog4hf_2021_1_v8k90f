﻿// <copyright file="EditorViaWindow.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GameDataBaseFF.Data;
    using GameWPF.BL;

    /// <summary>
    /// Class that implements the edit method.
    /// </summary>
    public class EditorViaWindow : IEditorService
    {
        /// <summary>
        /// The edit method that will modify a game with a window.
        /// </summary>
        /// <param name="g">The game that will be edited.</param>
        /// <returns>Wheter the modification was successful or not.</returns>
        public bool EditGame(Game g)
        {
            GameEditorWindow win = new GameEditorWindow(g);
            return win.ShowDialog() ?? false;
        }
    }
}
