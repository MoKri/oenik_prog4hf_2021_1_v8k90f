﻿// <copyright file="IntToCompanyConverter.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF.UI
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Data;

    /// <summary>
    /// Converter class that will return company names or their ids.
    /// </summary>
    public class IntToCompanyConverter : IValueConverter
    {
        /// <summary>
        /// Converts ints into company names.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>A company name as a string.</returns>
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            int num = (int)value;
            switch (num)
            {
                default:
                case 1: return "CD Projekt Red";
                case 2: return "Rockstar Games";
                case 3: return "Ubisoft Monrtreal";
                case 4: return "Santa Monica Studios";
                case 5: return "Rare";
                case 6: return "Nintendo";
                case 7: return "Sony";
                case 8: return "Microsoft";
            }
        }

        /// <summary>
        /// Converts company names into ints.
        /// </summary>
        /// <param name="value">The date that will be converted.</param>
        /// <param name="targetType">What the type of the target is.</param>
        /// <param name="parameter">The parameter that we want to get.</param>
        /// <param name="culture">Info about the system.</param>
        /// <returns>An int.</returns>
        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            string val = (string)value;

            switch (val)
            {
                default:
                case "CD Projekt Red": return 1;
                case "Rockstar Games":
                    return 2;
                case "Ubisoft Monrtreal":
                    return 3;
                case "Santa Monica Studios":
                    return 4;
                case "Rare":
                    return 5;
                case "Nintendo":
                    return 6;
                case "Sony":
                    return 7;
                case "Microsoft":
                    return 8;
            }
        }
    }
}
