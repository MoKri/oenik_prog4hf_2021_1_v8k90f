﻿// <copyright file="MyIoc.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameWPF
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight.Ioc;

    /// <summary>
    /// Class that is used as a serviceprovider.
    /// </summary>
    public class MyIoc
        : SimpleIoc, IServiceLocator
    {
        /// <summary>
        /// Gets an instance of the ioc.
        /// </summary>
        public static MyIoc Instance { get; private set; } = new MyIoc();
    }
}
