﻿// <copyright file="Company.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;

    /// <summary>
    /// Company entity that is used in the database for companies.
    /// Each of them is a developer of Games.
    /// </summary>
    public class Company
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Company"/> class.
        /// Also sets the Games, and Platforms virtual proerties so they wouldn't be null.
        /// </summary>
        public Company()
        {
            this.Games = new HashSet<Game>();
            this.Platforms = new HashSet<Platform>();
        }

        /// <summary>
        /// Gets or Sets the Company's Id, that is also the Primary key.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or Sets the Company's name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets the date, when the Company was founded.
        /// </summary>
        public DateTime FoundDate { get; set; }

        /// <summary>
        /// Gets or Sets the city in which the company's headquarters can be located.
        /// </summary>
        public string HeadquartersCity { get; set; }

        /// <summary>
        /// Gets or Sets the country in which the company's headquarters can be located.
        /// </summary>
        public string HeadquartersCountry { get; set; }

        /// <summary>
        /// Gets or Sets the name of the Company's CEO.
        /// </summary>
        public string CEO { get; set; }

        /// <summary>
        /// Gets or sets the Navigational property collection for Game entities.
        /// </summary>
        public virtual ICollection<Game> Games { get; set; }

        /// <summary>
        /// Gets the Navigational property collection for Platform entities.
        /// </summary>
        public virtual ICollection<Platform> Platforms { get; }

        /// <summary>
        /// Returns the datas of the Company entity.
        /// </summary>
        /// <returns>A string that contains the datas of the company entity.</returns>
        public override string ToString()
        {
            return $">> {this.Id} >> {this.Name} >> {this.FoundDate.ToShortDateString()} >> {this.HeadquartersCity} >> {this.HeadquartersCountry} >> {this.CEO}";
        }

        /// <summary>
        /// A method that could tell whether the companies are equal or not.
        /// </summary>
        /// <param name="obj">An another company.</param>
        /// <returns>Whether the two companies are equal or not.</returns>
        public override bool Equals(object obj)
        {
            Company c = obj as Company;

            return c != null && this.Name == c.Name && this.FoundDate == c.FoundDate && this.HeadquartersCity == c.HeadquartersCity && this.HeadquartersCountry == c.HeadquartersCountry && this.CEO == c.CEO;
        }

        /// <summary>
        /// A method that generates a Hash code for the company.
        /// </summary>
        /// <returns>The company's hashcode that the method generated.</returns>
        public override int GetHashCode()
        {
            return (this.Name.Length * 5) + 256 - this.HeadquartersCountry.Length;
        }
    }
}
