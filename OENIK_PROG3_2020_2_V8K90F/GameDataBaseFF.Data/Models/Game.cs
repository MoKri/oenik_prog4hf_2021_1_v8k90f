﻿// <copyright file="Game.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using GalaSoft.MvvmLight;
    using GameDataBaseFF.Data.Models;

    /// <summary>
    /// Game entity that is used in the database for games.
    /// </summary>
    public class Game : ObservableObject
    {
        /// <summary>
        /// the Primary key.
        /// </summary>
        private int id;

        /// <summary>
        /// the Game's name.
        /// </summary>
        private string name;

        /// <summary>
        /// the date of the Game's release.
        /// </summary>
        private DateTime releaseDate;

        /// <summary>
        /// how much the Game costs.
        /// </summary>
        private int price;

        /// <summary>
        /// what type of genre the Game belongs in.
        /// </summary>
        private string genre;

        /// <summary>
        /// what the type of the Game is. In this case("FPS","TPS").
        /// </summary>
        private string type;

        /// <summary>
        /// the Foreign key that references a Company's Primary key.
        /// </summary>
        private int companyId;

        /// <summary>
        /// the Foreign key that references a Platform's Primary key.
        /// </summary>
        private int platformId;

        /// <summary>
        /// Initializes a new instance of the <see cref="Game"/> class.
        /// </summary>
        public Game()
        {
        }

        /// <summary>
        /// Gets or Sets the Game's Id, that is also the Primary key.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get => this.id; set => this.Set(ref this.id, value); }

        /// <summary>
        /// Gets or Sets the Game's name.
        /// </summary>
        public string Name { get => this.name; set => this.Set(ref this.name, value); }

        /// <summary>
        /// Gets or Sets the date of the Game's release.
        /// </summary>
        public DateTime ReleaseDate { get => this.releaseDate; set => this.Set(ref this.releaseDate, value); }

        /// <summary>
        /// Gets or Sets how much the Game costs.
        /// </summary>
        public int Price { get => this.price; set => this.Set(ref this.price, value); }

        /// <summary>
        /// Gets or Sets what type of genre the Game belongs in.
        /// </summary>
        public string Genre { get => this.genre; set => this.Set(ref this.genre, value); }

        /// <summary>
        /// Gets or Sets what the type of the Game is. In this case("FPS","TPS").
        /// </summary>
        public string Type { get => this.type; set => this.Set(ref this.type, value); }

        /// <summary>
        /// Gets or Sets the Foreign key that references a Company's Primary key. In this case the company is the developer of the game.
        /// </summary>
        [ForeignKey(nameof(Company))]
        public int CompanyId { get => this.companyId; set => this.Set(ref this.companyId, value); }

        /// <summary>
        /// Gets or Sets the Foreign key that references a Platform's Primary key. This is the platform where the game is released.
        /// </summary>
        [ForeignKey(nameof(Platform))]
        public int PlatformId { get => this.platformId; set => this.Set(ref this.platformId, value); }

        /// <summary>
        /// Gets or Sets the navigational property for a Company entity.
        /// </summary>
        public virtual Company Company { get; set; }

        /// <summary>
        /// Gets or Sets the Navigational property for a Platform entity.
        /// </summary>
        public virtual Platform Platform { get; set; }

        /// <summary>
        /// Returns the datas of the Game entity.
        /// </summary>
        /// <returns>A string that contains the datas of the game entity.</returns>
        public override string ToString()
        {
            return $">> {this.Id} >> {this.Name} >> {this.ReleaseDate.ToShortDateString()} >> {this.Price} >> {this.Genre} >> {this.Type} >> {this.CompanyId} >> {this.PlatformId}";
        }

        /// <summary>
        /// Copies the datas of another game into this game.
        /// </summary>
        /// <param name="g">An other instance of a game.</param>
        public void CopyFrom(Game g)
        {
            if (g != null)
            {
                this.Id = g.Id;
                this.Name = g.Name;
                this.ReleaseDate = g.ReleaseDate;
                this.Price = g.Price;
                this.Genre = g.Genre;
                this.Type = g.Type;
                this.CompanyId = g.CompanyId;
                this.PlatformId = g.PlatformId;
                this.Company = g.Company;
            }
        }
    }
}
