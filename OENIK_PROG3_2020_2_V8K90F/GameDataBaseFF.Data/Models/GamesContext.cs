﻿// <copyright file="GamesContext.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Data.Models
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// This class is used for the generation, and access of the database.
    /// </summary>
    public class GamesContext : DbContext
    {
        /// <summary>
        ///  Initializes a new instance of the <see cref="GamesContext"/> class.
        ///  Also ensures that the database is created.
        /// </summary>
        public GamesContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or Sets the set that contains Game entities.
        /// </summary>
        public virtual DbSet<Game> Games { get; set; }

        /// <summary>
        /// Gets or Sets the set that contains Company entities.
        /// </summary>
        public virtual DbSet<Company> Companies { get; set; }

        /// <summary>
        /// Gets or Sets the set that contains Platform entities.
        /// </summary>
        public virtual DbSet<Platform> Platforms { get; set; }

        /// <summary>
        /// Sets where the database is located, and configures how it should be behaving.
        /// </summary>
        /// <param name="optionsBuilder">This is used to configure the database.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder != null && !optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies().UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\GameDb.mdf;Integrated Security=True;MultipleActiveResultSets=true");
            }
        }

        /// <summary>
        /// Sets up the connections between the database's entities, and fills them up with data.
        /// </summary>
        /// <param name="modelBuilder">This defines how the database entities should be behaving, and fills them up with data.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            Game g1 = new Game() { Id = 1, Name = "Cyberpunk 2077", ReleaseDate = new DateTime(2020, 11, 19), Price = 19000, Genre = "cyberpunk/shooter", Type = "FPS" };
            Game g2 = new Game() { Id = 2, Name = "Red Dead Redemption 2", ReleaseDate = new DateTime(2018, 10, 26), Price = 12000, Genre = "open-world/shooter/wild-west", Type = "TPS" };
            Game g3 = new Game() { Id = 3, Name = "God of War (2018)", ReleaseDate = new DateTime(2018, 04, 20), Price = 6500, Genre = "hack-n-slash/mythology", Type = "TPS" };
            Game g4 = new Game() { Id = 4, Name = "Assassin's Creed Valhalla", ReleaseDate = new DateTime(2020, 11, 10), Price = 21000, Genre = "open-world/history", Type = "TPS" };
            Game g5 = new Game() { Id = 5, Name = "Donkey Kong Country", ReleaseDate = new DateTime(1994, 11, 24), Price = 1500, Genre = "platformer", Type = "TPS" };
            Game g6 = new Game() { Id = 6, Name = "Assassin's Creed Origins", ReleaseDate = new DateTime(2017, 11, 10), Price = 10000, Genre = "open-world/history", Type = "TPS" };
            Game g7 = new Game() { Id = 7, Name = "Assassin's Creed Unity", ReleaseDate = new DateTime(2014, 11, 10), Price = 6000, Genre = "open-world/history", Type = "TPS" };
            Game g8 = new Game() { Id = 8, Name = "Spider-Man : Miles Morales", ReleaseDate = new DateTime(2020, 11, 12), Price = 30000, Genre = "open-world/action-adventure", Type = "TPS" };
            Game g9 = new Game() { Id = 9, Name = "Assassin's Creed Syndicate", ReleaseDate = new DateTime(2015, 11, 10), Price = 8000, Genre = "open-world/history", Type = "TPS" };
            Game g10 = new Game() { Id = 10, Name = "Far Cry 4", ReleaseDate = new DateTime(2014, 11, 18), Price = 6000, Genre = "open-world/shooter", Type = "FPS" };
            Game g11 = new Game() { Id = 11, Name = "Far Cry 5", ReleaseDate = new DateTime(2018, 03, 27), Price = 10000, Genre = "open-world/shooter", Type = "FPS" };
            Game g12 = new Game() { Id = 12, Name = "The Witcher III : Wild Hunt REMASTERED", ReleaseDate = new DateTime(2021, 05, 18), Price = 22000, Genre = "open-world/RPG/Fantasy", Type = "TPS" };
            Game g13 = new Game() { Id = 13, Name = "GTA V", ReleaseDate = new DateTime(2021, 09, 21), Price = 25000, Genre = "open-world/shooter", Type = "TPS" };

            Company c1 = new Company() { Id = 1, Name = "CD Projekt Red", FoundDate = new DateTime(1994, 05, 01), HeadquartersCity = "Warsaw", HeadquartersCountry = "Poland", CEO = "Marcin Iwiński" };
            Company c2 = new Company() { Id = 2, Name = "Rockstar Games", FoundDate = new DateTime(1998, 12, 01), HeadquartersCity = "New York", HeadquartersCountry = "United States", CEO = "Sam Houser" };
            Company c3 = new Company() { Id = 3, Name = "Ubisoft Montreal", FoundDate = new DateTime(1997, 04, 25), HeadquartersCity = "Montreal", HeadquartersCountry = "Canada", CEO = "Yves Guillemot" };
            Company c4 = new Company() { Id = 4, Name = "Santa Monica Studio", FoundDate = new DateTime(1999, 09, 12), HeadquartersCity = "Los Angeles", HeadquartersCountry = "United States", CEO = "Yumi Yang" };
            Company c5 = new Company() { Id = 5, Name = "Rare", FoundDate = new DateTime(1985, 10, 11), HeadquartersCity = "Leicestershire", HeadquartersCountry = "England", CEO = "Craig Duncan" };
            Company c6 = new Company() { Id = 6, Name = "Nintendo", FoundDate = new DateTime(1989, 09, 23), HeadquartersCity = "Kiotó", HeadquartersCountry = "Japan", CEO = "Shuntaro Furukawa" };
            Company c7 = new Company() { Id = 7, Name = "Sony", FoundDate = new DateTime(1993, 11, 16), HeadquartersCity = "San Mateo", HeadquartersCountry = "United States", CEO = "Josida Kenicsiro" };
            Company c8 = new Company() { Id = 8, Name = "Microsoft", FoundDate = new DateTime(1975, 04, 04), HeadquartersCity = "Redmond", HeadquartersCountry = "United States", CEO = "Satya Nadella" };

            Platform p1 = new Platform() { Id = 1, Name = "Ps4", ReleaseDate = new DateTime(2013, 11, 15), Price = 120000, Storage = 500, HDMI = true };
            Platform p2 = new Platform() { Id = 2, Name = "Ps5", ReleaseDate = new DateTime(2020, 11, 12), Price = 160000, Storage = 825, HDMI = true };
            Platform p3 = new Platform() { Id = 3, Name = "Xbox One", ReleaseDate = new DateTime(2013, 11, 22), Price = 120000, Storage = 1000, HDMI = true };
            Platform p4 = new Platform() { Id = 4, Name = "Xbox Series X", ReleaseDate = new DateTime(2020, 11, 10), Price = 185000, Storage = 1000, HDMI = true };
            Platform p5 = new Platform() { Id = 5, Name = "GameBoy Color", ReleaseDate = new DateTime(1998, 11, 23), Price = 32000, Storage = 0, HDMI = false };

            g1.CompanyId = c1.Id;
            g1.PlatformId = p4.Id;

            g2.CompanyId = c2.Id;
            g2.PlatformId = p1.Id;

            g3.CompanyId = c4.Id;
            g3.PlatformId = p1.Id;

            g4.CompanyId = c3.Id;
            g4.PlatformId = p2.Id;

            g5.CompanyId = c5.Id;
            g5.PlatformId = p5.Id;

            g6.CompanyId = c3.Id;
            g6.PlatformId = p1.Id;

            g7.CompanyId = c3.Id;
            g7.PlatformId = p3.Id;

            g8.CompanyId = c7.Id;
            g8.PlatformId = p2.Id;

            g9.CompanyId = c3.Id;
            g9.PlatformId = p3.Id;

            g10.CompanyId = c3.Id;
            g10.PlatformId = p3.Id;

            g11.CompanyId = c3.Id;
            g11.PlatformId = p1.Id;

            g12.CompanyId = c1.Id;
            g12.PlatformId = p4.Id;

            g13.CompanyId = c2.Id;
            g13.PlatformId = p4.Id;

            p1.ProducerId = c7.Id;
            p2.ProducerId = c7.Id;
            p3.ProducerId = c8.Id;
            p4.ProducerId = c8.Id;
            p5.ProducerId = c6.Id;

            if (modelBuilder != null)
            {
                modelBuilder.Entity<Game>(entity =>
                {
                    entity.HasOne(game => game.Company)
                          .WithMany(company => company.Games)
                          .HasForeignKey(game => game.CompanyId)
                          .OnDelete(DeleteBehavior.ClientSetNull);
                });

                modelBuilder.Entity<Game>(entity =>
                {
                    entity.HasOne(game => game.Platform)
                          .WithMany(platform => platform.Games)
                          .HasForeignKey(game => game.PlatformId)
                          .OnDelete(DeleteBehavior.ClientSetNull);
                });

                modelBuilder.Entity<Platform>(entity =>
                {
                    entity.HasOne(platform => platform.Producer)
                          .WithMany(company => company.Platforms)
                          .HasForeignKey(platform => platform.ProducerId)
                          .OnDelete(DeleteBehavior.ClientSetNull);
                });

                modelBuilder.Entity<Game>().HasData(g1, g2, g3, g4, g5, g6, g7, g8, g9, g10, g11, g12, g13);
                modelBuilder.Entity<Company>().HasData(c1, c2, c3, c4, c5, c6, c7, c8);
                modelBuilder.Entity<Platform>().HasData(p1, p2, p3, p4, p5);
            }
        }
    }
}
