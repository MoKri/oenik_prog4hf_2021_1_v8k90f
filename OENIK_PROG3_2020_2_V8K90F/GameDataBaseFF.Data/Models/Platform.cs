﻿// <copyright file="Platform.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Data
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Text;
    using GameDataBaseFF.Data.Models;

    /// <summary>
    /// Platform entity that is used in the database for platforms.
    /// Each of them is a device that can be used for playing games.
    /// </summary>
    public class Platform
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Platform"/> class.
        /// Also sets the Games virtual proerty so it wouldn't be null.
        /// </summary>
        public Platform()
        {
            this.Games = new HashSet<Game>();
        }

        /// <summary>
        /// Gets or Sets the Platform's Id, that is also the Primary key.
        /// </summary>
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// Gets or Sets what the Platform's name is.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or Sets the date on which the device was released.
        /// </summary>
        public DateTime ReleaseDate { get; set; }

        /// <summary>
        /// Gets or Sets how much the Platform costs.
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Gets or Sets the value that defines how much data can be stored on the Platform.
        /// </summary>
        public int Storage { get; set; }

        /// <summary>
        /// Gets or Sets a value indicating whether there's an HDMI port on the device.
        /// </summary>
        public bool HDMI { get; set; }

        /// <summary>
        /// Gets or Sets the Foreign key that references a Company's Primary key. In this case the company is the one who produced this device.
        /// </summary>
        [ForeignKey(nameof(Producer))]
        public int ProducerId { get; set; }

        /// <summary>
        /// Gets or Sets the navigational property for a Company entity.
        /// </summary>
        public virtual Company Producer { get; set; }

        /// <summary>
        ///  Gets the Navigational property collection for Game entities.
        /// </summary>
        public virtual ICollection<Game> Games { get; }

        /// <summary>
        /// Returns the datas of the Platform entity.
        /// </summary>
        /// <returns>A string that contains the datas of the platform entity.</returns>
        public override string ToString()
        {
            return $">> {this.Id} >> {this.Name} >> {this.ReleaseDate.ToShortDateString()} >> {this.Price} >> {this.Storage} >> {this.HDMI} >> {this.ProducerId}";
        }
    }
}
