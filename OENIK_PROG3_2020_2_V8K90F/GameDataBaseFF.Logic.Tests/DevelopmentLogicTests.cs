﻿// <copyright file="DevelopmentLogicTests.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GameDataBaseFF.Data;
    using GameDataBaseFF.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// A class that is used for testing the DevelopmentLogic class.
    /// </summary>
    [TestFixture]
    public class DevelopmentLogicTests
    {
        private Mock<ICompanyRepository> compRepo;
        private Mock<IGameRepository> gameRepo;
        private DevelopmentLogic logic;

        /// <summary>
        /// Tests whether the Create method of the CRUD methods works as intended.
        /// </summary>
        [Test]
        public void TestAddingANewCompany()
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.gameRepo = new Mock<IGameRepository>(MockBehavior.Loose);

            Company testcase = new Company() { Name = "Test Company", FoundDate = DateTime.Now, HeadquartersCity = "SomeCity", HeadquartersCountry = "SomeCountry", CEO = "SomeCEO" };

            this.compRepo.Setup(repo => repo.Insert(testcase));

            this.logic = new DevelopmentLogic(this.gameRepo.Object, this.compRepo.Object);
            this.logic.CreateCompany(testcase.Name, testcase.FoundDate, testcase.HeadquartersCity, testcase.HeadquartersCountry, testcase.CEO);

            this.compRepo.Verify(repo => repo.Insert(testcase), Times.Once);
            this.compRepo.Verify(repo => repo.Insert(It.IsAny<Company>()), Times.Once);
            this.gameRepo.Verify(repo => repo.Insert(It.IsAny<Game>()), Times.Never);
        }

        /// <summary>
        /// Tests whether the Read one method of the CRUD methods works as intended.
        /// </summary>
        /// <param name="id">A number that is the id of the game that we want to get.</param>
        [TestCase(1)]
        [TestCase(16)]
        [TestCase(19)]
        public void TestGettingOnlyOneGame(int id)
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.gameRepo = new Mock<IGameRepository>(MockBehavior.Loose);

            Game g = new Game() { Id = 1, Name = "Cyberpunk 2077", ReleaseDate = new DateTime(2020, 11, 19), Price = 19000, Genre = "cyberpunk/shooter", Type = "FPS" };

            this.gameRepo.Setup(repo => repo.GetOne(It.IsAny<int>())).Returns(g);
            this.logic = new DevelopmentLogic(this.gameRepo.Object, this.compRepo.Object);
            Game expected = this.logic.GetOneGame(id);

            this.gameRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Once);
            this.compRepo.Verify(repo => repo.GetOne(It.IsAny<int>()), Times.Never);
        }

        /// <summary>
        /// Tests whether the Delete method of the CRUD methods works as intended.
        /// </summary>
        [Test]
        public void TestThatRemovesAGame()
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.gameRepo = new Mock<IGameRepository>(MockBehavior.Loose);

            this.gameRepo.Setup(repo => repo.Remove(It.IsAny<Game>())).Returns(It.IsAny<bool>());
            this.logic = new DevelopmentLogic(this.gameRepo.Object, this.compRepo.Object);

            Game delete = new Game();
            this.logic.RemoveGame(delete);
            this.logic.RemoveGame(new Game());

            this.gameRepo.Verify(repo => repo.Remove(delete), Times.Once);
            this.gameRepo.Verify(repo => repo.Remove(It.IsAny<Game>()), Times.Exactly(2));
        }

        // NON-CRUD TESTS

        /// <summary>
        /// Tests whether the AverageGamePrice non-CRUD method works as intended.
        /// </summary>
        [Test]
        public void TestThatGameAverageIsAsExpected()
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.gameRepo = new Mock<IGameRepository>(MockBehavior.Loose);

            List<Game> games = new List<Game>()
            {
                new Game() { Name = "Test 1", Price = 10000 },
                new Game() { Name = "Test 1", Price = 5000 },
            };

            this.gameRepo.Setup(repo => repo.GetAll()).Returns(games.AsQueryable);
            this.logic = new DevelopmentLogic(this.gameRepo.Object, this.compRepo.Object);

            Assert.That(this.logic.AverageGamePrice(), Is.EqualTo(7500));
            this.gameRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests whether the GamesByCompanies non-CRUD method works as intended.
        /// </summary>
        [Test]
        public void TestThatGamesAreCorrectlyListedWithTheirDevelopmentCompany()
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.gameRepo = new Mock<IGameRepository>(MockBehavior.Loose);

            List<Company> companies = new List<Company>()
            {
                new Company()
                {
                    Name = "Company1",
                    Games = new List<Game>()
                    {
                        new Game() { Name = "Game1" },
                        new Game() { Name = "Game2" },
                    },
                },
                new Company()
                {
                    Name = "Company2",
                    Games = new List<Game>()
                    {
                        new Game() { Name = "Game3" },
                    },
                },
            };

            List<GameByCompany> expected = new List<GameByCompany>()
            {
                new GameByCompany() { CompanyName = companies[0].Name, GameTitles = string.Join(',', companies[0].Games.Select(x => x.Name)) },
                new GameByCompany() { CompanyName = companies[1].Name, GameTitles = string.Join(',', companies[1].Games.Select(x => x.Name)) },
            };

            this.compRepo.Setup(repo => repo.GetAll()).Returns(companies.AsQueryable);
            this.logic = new DevelopmentLogic(this.gameRepo.Object, this.compRepo.Object);

            var test = this.logic.GamesByCompanies();

            Assert.That(test, Is.EqualTo(expected));

            this.compRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests whether the SumGamesPriceByCompany non-CRUD method works as intended.
        /// </summary>
        [Test]
        public void TestThatSumGamesByCompanyReturnsTheRightSums()
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.gameRepo = new Mock<IGameRepository>(MockBehavior.Loose);

            List<Company> companies = new List<Company>()
            {
                new Company() { Id = 1, Name = "TestCompany 1" },
                new Company() { Id = 2, Name = "TestCompany 2" },
            };

            List<Game> games = new List<Game>()
            {
                new Game() { Name = "TestGame1", Price = 15000, CompanyId = 1 },
                new Game() { Name = "TestGame2", Price = 8000, CompanyId = 1 },
                new Game() { Name = "TestGame3", Price = 3000, CompanyId = 2 },
                new Game() { Name = "TestGame4", Price = 3000, CompanyId = 2 },
                new Game() { Name = "TestGame5", Price = 3000, CompanyId = 2 },
            };

            List<GamePriceByCompany> expected = new List<GamePriceByCompany>()
            {
                new GamePriceByCompany() { CompanyName = companies[0].Name, SumPrice = games[0].Price + games[1].Price },
                new GamePriceByCompany() { CompanyName = companies[1].Name, SumPrice = games[2].Price + games[3].Price + games[4].Price },
            };

            this.compRepo.Setup(repo => repo.GetAll()).Returns(companies.AsQueryable);
            this.gameRepo.Setup(repo => repo.GetAll()).Returns(games.AsQueryable);
            this.logic = new DevelopmentLogic(this.gameRepo.Object, this.compRepo.Object);

            var test = this.logic.SumGamesPriceByCompany();

            Assert.That(test, Is.EqualTo(expected));

            this.compRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.gameRepo.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
