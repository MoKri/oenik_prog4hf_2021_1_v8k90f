﻿// <copyright file="DeviceProduceLogicTests.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GameDataBaseFF.Data;
    using GameDataBaseFF.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// A class that is used for testing the DeviceProduceLogic class.
    /// </summary>
    [TestFixture]
    public class DeviceProduceLogicTests
    {
        private Mock<ICompanyRepository> compRepo;
        private Mock<IPlatformRepository> platRepo;
        private DeviceProduceLogic logic;

        /// <summary>
        /// Tests whether the Read all method of the CRUD methods works as intended.
        /// </summary>
        [Test]
        public void TestThatGetsAllOfThePlatforms()
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.platRepo = new Mock<IPlatformRepository>(MockBehavior.Loose);

            List<Platform> plats = new List<Platform>()
            {
                new Platform() { Name = "Platform number #1" },
                new Platform() { Name = "Platform number #2" },
                new Platform() { Name = "Platform number #3" },
            };

            this.platRepo.Setup(repo => repo.GetAll()).Returns(plats.AsQueryable);
            this.logic = new DeviceProduceLogic(this.compRepo.Object, this.platRepo.Object);

            List<Platform> expected = (List<Platform>)this.logic.GetAllPlatforms();

            this.platRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests whether the Update method of the CRUD methods works as intended.
        /// </summary>
        [Test]
        public void TestThatUpdatesACompanysCEO()
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.platRepo = new Mock<IPlatformRepository>(MockBehavior.Loose);

            this.compRepo.Setup(repo => repo.ChangeBoss(It.IsAny<int>(), It.IsAny<string>()));
            this.logic = new DeviceProduceLogic(this.compRepo.Object, this.platRepo.Object);

            this.logic.ChangeCompanyBoss(1, "Test-Boss");

            this.compRepo.Verify(repo => repo.ChangeBoss(It.IsAny<int>(), It.IsAny<string>()), Times.Once);
        }

        // NON-Crud-Tests

        /// <summary>
        /// Tests whether the NonHDMIPlatforms non-CRUD method works as intended.
        /// </summary>
        [Test]
        public void TestThatNonHDMIOnlyReturnsPlatformsWithNoHDMIPort()
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.platRepo = new Mock<IPlatformRepository>(MockBehavior.Loose);

            List<Platform> platforms = new List<Platform>()
            {
                new Platform() { Name = "Test 1", HDMI = false, },
                new Platform() { Name = "Test 2", HDMI = false, },
                new Platform() { Name = "Test 3", HDMI = false, },
                new Platform() { Name = "Test 4", HDMI = true, },
                new Platform() { Name = "Test 5", HDMI = false, },
                new Platform() { Name = "Test 6", HDMI = true, },
            };

            List<Platform> expected = new List<Platform>()
            {
                platforms[0],
                platforms[1],
                platforms[2],
                platforms[4],
            };

            this.platRepo.Setup(repo => repo.GetAll()).Returns(platforms.AsQueryable);
            this.logic = new DeviceProduceLogic(this.compRepo.Object, this.platRepo.Object);

            Assert.That(this.logic.NonHDMIPlatforms(), Is.EqualTo(expected));
            this.platRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests whether the CountPlatformByCompany non-CRUD method works as intended.
        /// </summary>
        [Test]
        public void TestThatCountPlatformByCompanyReturnsTheRightAmounts()
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.platRepo = new Mock<IPlatformRepository>(MockBehavior.Loose);

            List<Company> companies = new List<Company>()
            {
                new Company() { Id = 1, Name = "TestCompany1" },
                new Company() { Id = 2, Name = "TestCompany2" },
                new Company() { Id = 3, Name = "TestCompany3" },
            };

            List<Platform> platforms = new List<Platform>()
            {
                new Platform() { Name = "TestPlatform1", ProducerId = 1 },
                new Platform() { Name = "TestPlatform2", ProducerId = 1 },
                new Platform() { Name = "TestPlatform3", ProducerId = 2 },
                new Platform() { Name = "TestPlatform4", ProducerId = 3 },
                new Platform() { Name = "TestPlatform5", ProducerId = 3 },
                new Platform() { Name = "TestPlatform6", ProducerId = 3 },
            };

            List<PlatformNumsByCompany> expected = new List<PlatformNumsByCompany>()
            {
                new PlatformNumsByCompany() { CompanyName = companies[0].Name, CountNumber = 2 },
                new PlatformNumsByCompany() { CompanyName = companies[1].Name, CountNumber = 1 },
                new PlatformNumsByCompany() { CompanyName = companies[2].Name, CountNumber = 3 },
            };

            List<PlatformNumsByCompany> expectedbyorder = new List<PlatformNumsByCompany>()
            {
                new PlatformNumsByCompany() { CompanyName = companies[2].Name, CountNumber = 3 },
                new PlatformNumsByCompany() { CompanyName = companies[0].Name, CountNumber = 2 },
                new PlatformNumsByCompany() { CompanyName = companies[1].Name, CountNumber = 1 },
            };

            this.compRepo.Setup(repo => repo.GetAll()).Returns(companies.AsQueryable);
            this.platRepo.Setup(repo => repo.GetAll()).Returns(platforms.AsQueryable);
            this.logic = new DeviceProduceLogic(this.compRepo.Object, this.platRepo.Object);

            var test = this.logic.CountPlatformByCompany();

            Assert.That(test, Is.EqualTo(expectedbyorder));
            Assert.That(test, Is.EquivalentTo(expected));

            this.platRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.compRepo.Verify(repo => repo.GetAll(), Times.Once);
        }

        /// <summary>
        /// Tests whether the AVGPlatformCostByCompany non-CRUD method works as intended.
        /// </summary>
        [Test]
        public void TestThatAVGCostByCompanyReturnsTheCompanyWithTheCheapestAverage()
        {
            this.compRepo = new Mock<ICompanyRepository>(MockBehavior.Loose);
            this.platRepo = new Mock<IPlatformRepository>(MockBehavior.Loose);

            List<Company> companies = new List<Company>()
            {
                new Company() { Id = 1, Name = "TestCompany1" },
                new Company() { Id = 2, Name = "TestCompany2" },
                new Company() { Id = 3, Name = "TestCompany3" },
            };

            List<Platform> platforms = new List<Platform>()
            {
                new Platform() { Name = "TestPlatform1", Price = 120000, ProducerId = 1 },
                new Platform() { Name = "TestPlatform2", Price = 200000, ProducerId = 2 },
                new Platform() { Name = "TestPlatform3", Price = 250000, ProducerId = 2 },
                new Platform() { Name = "TestPlatform4", Price = 200000, ProducerId = 2 },
                new Platform() { Name = "TestPlatform5", Price = 600000, ProducerId = 3 },
                new Platform() { Name = "TestPlatform6", Price = 80000, ProducerId = 1 },
            };

            List<AveragePlatCostByComp> expectedorder = new List<AveragePlatCostByComp>()
            {
                new AveragePlatCostByComp() { CompanyName = companies[0].Name, AverageCost = 100000 },
                new AveragePlatCostByComp() { CompanyName = companies[1].Name, AverageCost = (double)(platforms[1].Price + platforms[2].Price + platforms[3].Price) / 3 },
                new AveragePlatCostByComp() { CompanyName = companies[2].Name, AverageCost = 600000 },
            };

            List<AveragePlatCostByComp> expected = new List<AveragePlatCostByComp>()
            {
                new AveragePlatCostByComp() { CompanyName = companies[2].Name, AverageCost = 600000 },
                new AveragePlatCostByComp() { CompanyName = companies[0].Name, AverageCost = 100000 },
                new AveragePlatCostByComp() { CompanyName = companies[1].Name, AverageCost = (double)(platforms[1].Price + platforms[2].Price + platforms[3].Price) / 3 },
            };

            this.compRepo.Setup(repo => repo.GetAll()).Returns(companies.AsQueryable);
            this.platRepo.Setup(repo => repo.GetAll()).Returns(platforms.AsQueryable);
            this.logic = new DeviceProduceLogic(this.compRepo.Object, this.platRepo.Object);

            var test = this.logic.AVGPlatformCostByCompany();

            Assert.That(test, Is.EqualTo(expectedorder));
            Assert.That(test, Is.EquivalentTo(expected));

            this.compRepo.Verify(repo => repo.GetAll(), Times.Once);
            this.platRepo.Verify(repo => repo.GetAll(), Times.Once);
        }
    }
}
