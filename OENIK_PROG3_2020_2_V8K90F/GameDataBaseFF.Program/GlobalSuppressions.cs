﻿// <copyright file="GlobalSuppressions.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I have to convert a string into an int.", Scope = "member", Target = "~M:GameDataBaseFF.Program.MainPrg.GamesMenu(GameDataBaseFF.Logic.DevelopmentLogic)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I have to convert a string into an int.", Scope = "member", Target = "~M:GameDataBaseFF.Program.MainPrg.CompanyMenu(GameDataBaseFF.Logic.DevelopmentLogic)")]
[assembly: SuppressMessage("Globalization", "CA1305:Specify IFormatProvider", Justification = "I have to convert a string into an int.", Scope = "member", Target = "~M:GameDataBaseFF.Program.MainPrg.PlatformMenu(GameDataBaseFF.Logic.DeviceProduceLogic)")]
[assembly: SuppressMessage("Globalization", "CA1304:Specify CultureInfo", Justification = "I had to use ToLower on a string so it wouldn't make a difference between capital characters and normal characters.", Scope = "member", Target = "~M:GameDataBaseFF.Program.MainPrg.PlatformMenu(GameDataBaseFF.Logic.DeviceProduceLogic)")]
[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]