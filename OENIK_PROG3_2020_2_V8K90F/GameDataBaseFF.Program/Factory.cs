﻿// <copyright file="Factory.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Program
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GameDataBaseFF.Data.Models;
    using GameDataBaseFF.Logic;
    using GameDataBaseFF.Repository;

    /// <summary>
    /// Class that will instantiate other classes in Main().
    /// </summary>
    public static class Factory
    {
        private static GamesContext ctx = new GamesContext();

        /// <summary>
        /// Static method that is used to instantiate a GamesContext element.
        /// </summary>
        /// <returns>A GamesContext type element.</returns>
        public static GamesContext GamesContextGenerator()
        {
            return ctx;
        }

        /// <summary>
        /// Static method that is used to instantiate a GameRepository element.
        /// </summary>
        /// <returns>A Gamerepository type element.</returns>
        public static GameRepository GameRepoGenerator()
        {
            return new GameRepository(ctx);
        }

        /// <summary>
        /// Static method that is used to instantiate a CompanyRepository element.
        /// </summary>
        /// <returns>A CompanyRepository type element.</returns>
        public static CompanyRepository CompanyRepoGenerator()
        {
            return new CompanyRepository(ctx);
        }

        /// <summary>
        /// Static method that is used to instantiate a PlatformRepository element.
        /// </summary>
        /// <returns>A PlatformRepository type element.</returns>
        public static PlatformRepository PlatformRepoGenerator()
        {
            return new PlatformRepository(ctx);
        }

        /// <summary>
        /// Static method that is used to instantiate a DevelopmentLogic element.
        /// </summary>
        /// <param name="gr">GameRepository used in the constructor of DevelopmentLogic.</param>
        /// <param name="cr">CompanyRepository used in the constructor of DevelopmentLogic.</param>
        /// <returns>A DevelopmentLogic type element.</returns>
        public static DevelopmentLogic DevelopLogicGenerator(GameRepository gr, CompanyRepository cr)
        {
            return new DevelopmentLogic(gr, cr);
        }

        /// <summary>
        /// Static method that is used to instantiate a DeviceProduceLogic element.
        /// </summary>
        /// <param name="cr">CompanyRepository used in the constructor of DevelopmentLogic.</param>
        /// <param name="pr">PlatformRepository used in the constructor of DevelopmentLogic.</param>
        /// <returns>A DeviceProduceLogic type element.</returns>
        public static DeviceProduceLogic DeviceProdLogicGenerator(CompanyRepository cr, PlatformRepository pr)
        {
            return new DeviceProduceLogic(cr, pr);
        }
    }
}
