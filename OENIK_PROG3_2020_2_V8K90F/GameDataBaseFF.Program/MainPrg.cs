﻿// <copyright file="MainPrg.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Program
{
    using System;
    using ConsoleTools;
    using GameDataBaseFF.Data.Models;
    using GameDataBaseFF.Logic;
    using GameDataBaseFF.Repository;

    /// <summary>
    /// This will be the class where the program starts.
    /// </summary>
    public static class MainPrg
    {
        /// <summary>
        /// This is the Main method.
        /// </summary>
        public static void Main()
        {
            GamesContext ctx = Factory.GamesContextGenerator();
            GameRepository gRepo = Factory.GameRepoGenerator();
            CompanyRepository cRepo = Factory.CompanyRepoGenerator();
            PlatformRepository pRepo = Factory.PlatformRepoGenerator();
            DevelopmentLogic devLogic = Factory.DevelopLogicGenerator(gRepo, cRepo);
            DeviceProduceLogic prodLogic = Factory.DeviceProdLogicGenerator(cRepo, pRepo);

            var menu = new ConsoleMenu().Add("-- LIST ALL --", () => ListAll(devLogic, prodLogic))
                                        .Add("-- LIST GAMES --", () => ListGames(devLogic, false))
                                        .Add("-- LIST COMPANIES --", () => ListCompanies(devLogic, false))
                                        .Add("-- LIST PLATFORMS --", () => ListPlatforms(prodLogic, false))
                                        .Add("-- GAMES --", () => GamesMenu(devLogic))
                                        .Add("-- COMPANIES --", () => CompanyMenu(devLogic))
                                        .Add("-- PLATFORMS --", () => PlatformMenu(prodLogic))
                                        .Add("-- AVERAGE PRICE OF GAMES --", () => AveragePrice(devLogic))
                                        .Add("-- SHOW GAMES BY COMPANIES --", () => GamesByCompanies(devLogic))
                                        .Add("-- SHOW GAMES BY COMPANIES ASYNC --", () => GamesByCompaniesAsync(devLogic))
                                        .Add("-- SUM GAME PRICES BY COMPANIES --", () => SumGameByComp(devLogic))
                                        .Add("-- SUM GAME PRICES BY COMPANIES ASYNC --", () => SumGameByCompAsync(devLogic))
                                        .Add("-- PLATFORM NUMBERS BY COMPANIES --", () => PlatNumByComp(prodLogic))
                                        .Add("-- PLATFORM NUMBERS BY COMPANIES ASYNC --", () => PlatNumByCompAsync(prodLogic))
                                        .Add("-- CHEAPEST AVERAGE COST OF PLATFORMS BY COMPANIES --", () => CheapAvgPlat(prodLogic))
                                        .Add("-- CHEAPEST AVERAGE COST OF PLATFORMS BY COMPANIES ASYNC --", () => CheapAvgPlatAsync(prodLogic))
                                        .Add("-- PLATFORMS WITH NO HDMI PORTS --", () => NoHDMI(prodLogic))
                                        .Add("-- PLATFORMS WITH NO HDMI PORTS ASYNC --", () => NoHDMIAsync(prodLogic))
                                        .Add("-- QUIT --", ConsoleMenu.Close);

            menu.Show();
        }

        /// <summary>
        /// Static method that shows the name of those platforms, that does not support an hdmi cable, but it uses the async version of the method.
        /// </summary>
        /// <param name="prodLogic">DeviceProduceLogic type parameter.</param>
        public static void NoHDMIAsync(DeviceProduceLogic prodLogic)
        {
            if (prodLogic != null)
            {
                foreach (var item in prodLogic.NonHDMIPlatformsAsync().Result)
                {
                    Console.WriteLine(item.Name);
                }
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Static method that shows which company has the cheapest platforms by average, but it uses the async version of the method.
        /// </summary>
        /// <param name="prodLogic">DeviceProduceLogic type parameter.</param>
        public static void CheapAvgPlatAsync(DeviceProduceLogic prodLogic)
        {
            if (prodLogic != null)
            {
                var item = prodLogic.AVGPlatformCostByCompanyAsync().Result;
                Console.WriteLine($"[{item[0].CompanyName}] : {item[0].AverageCost} Ft");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Static method that lists how many consoles each company has made so far, but it uses the async version of the method.
        /// </summary>
        /// <param name="prodLogic">DeviceProduceLogic type parameter.</param>
        public static void PlatNumByCompAsync(DeviceProduceLogic prodLogic)
        {
            if (prodLogic != null)
            {
                foreach (var item in prodLogic.CountPlatformByCompanyAsync().Result)
                {
                    Console.WriteLine($"[{item.CompanyName}] : {item.CountNumber}\n");
                }
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Static method that lists how much money the developer company's games are worth, but it uses the async version of the method.
        /// </summary>
        /// <param name="devLogic">DevelopmentLogic type parameter.</param>
        public static void SumGameByCompAsync(DevelopmentLogic devLogic)
        {
            if (devLogic != null)
            {
                foreach (var item in devLogic.SumGamesPriceByCompanyAsync().Result)
                {
                    Console.WriteLine($"[{item.CompanyName}] -- Sum: {item.SumPrice} Ft\n");
                }
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Static method that lists the name of the games by their developers, but it uses the async version of the method.
        /// </summary>
        /// <param name="devLogic">DevelopmentLogic type parameter.</param>
        public static void GamesByCompaniesAsync(DevelopmentLogic devLogic)
        {
            if (devLogic != null)
            {
                foreach (var item in devLogic.GamesByCompaniesAsync().Result)
                {
                    Console.WriteLine($"[{item.CompanyName}] : {item.GameTitles}\n");
                }
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Static method that shows which company has the cheapest platforms by average.
        /// </summary>
        /// <param name="prodLogic">DeviceProduceLogic type parameter.</param>
        public static void CheapAvgPlat(DeviceProduceLogic prodLogic)
        {
            if (prodLogic != null)
            {
                var item = prodLogic.AVGPlatformCostByCompany();
                Console.WriteLine($"[{item[0].CompanyName}] : {item[0].AverageCost} Ft");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Static method that shows the name of those platforms, that does not support an hdmi cable.
        /// </summary>
        /// <param name="prodLogic">DeviceProduceLogic type parameter.</param>
        public static void NoHDMI(DeviceProduceLogic prodLogic)
        {
            if (prodLogic != null)
            {
                foreach (var item in prodLogic.NonHDMIPlatforms())
                {
                    Console.WriteLine(item.Name);
                }
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Static method that lists how much money the developer company's games are worth.
        /// </summary>
        /// <param name="devLogic">DevelopmentLogic type parameter.</param>
        public static void SumGameByComp(DevelopmentLogic devLogic)
        {
            if (devLogic != null)
            {
                foreach (var item in devLogic.SumGamesPriceByCompany())
                {
                    Console.WriteLine($"[{item.CompanyName}] -- Sum: {item.SumPrice} Ft\n");
                }

                Console.ReadLine();
            }
        }

        /// <summary>
        /// Static method that lists how many consoles each company has made so far.
        /// </summary>
        /// <param name="prodLogic">DeviceProduceLogic type parameter.</param>
        public static void PlatNumByComp(DeviceProduceLogic prodLogic)
        {
            if (prodLogic != null)
            {
                foreach (var item in prodLogic.CountPlatformByCompany())
                {
                    Console.WriteLine($"[{item.CompanyName}] : {item.CountNumber}\n");
                }

                Console.ReadLine();
            }
        }

        /// <summary>
        /// Static method that is used to list all the table's elements from the database.
        /// </summary>
        /// <param name="dlog">DevelopmentLogic type parameter.</param>
        /// <param name="plog">DeviceProduceLogic type parameter.</param>
        public static void ListAll(DevelopmentLogic dlog, DeviceProduceLogic plog)
        {
            ListGames(dlog, true);
            Console.WriteLine();
            ListCompanies(dlog, true);
            Console.WriteLine();
            ListPlatforms(plog, true);
            Console.ReadLine();
        }

        /// <summary>
        /// Static method that is used to list all the Game entities from the database. The bool parameter is used to identify wheteher we want to list everything or only one table.
        /// </summary>
        /// <param name="logic">DevelopmentLogic type parameter.</param>
        /// <param name="all">Bool type parameter.</param>
        public static void ListGames(DevelopmentLogic logic, bool all)
        {
            if (logic != null)
            {
                foreach (var item in logic.GetAllGames())
                {
                    Console.WriteLine(item.Name);
                }
            }

            if (!all)
            {
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Static method that is used to list all the Company entities from the database. The bool parameter is used to identify wheteher we want to list everything or only one table.
        /// </summary>
        /// <param name="logic">DevelopmentLogic type parameter.</param>
        /// <param name="all">Bool type parameter.</param>
        public static void ListCompanies(DevelopmentLogic logic, bool all)
        {
            if (logic != null)
            {
                foreach (var item in logic.GetAllCompanies())
                {
                    Console.WriteLine(item.Name);
                }
            }

            if (!all)
            {
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Static method that is used to list all the Platform entities from the database. The bool parameter is used to identify wheteher we want to list everything or only one table.
        /// </summary>
        /// <param name="logic">DeviceProduceLogic type parameter.</param>
        /// <param name="all">Bool type parameter.</param>
        public static void ListPlatforms(DeviceProduceLogic logic, bool all)
        {
            if (logic != null)
            {
                foreach (var item in logic.GetAllPlatforms())
                {
                    Console.WriteLine(item.Name);
                }
            }

            if (!all)
            {
                Console.ReadLine();
            }
        }

        /// <summary>
        /// Static method that shows what the average price of the games are in the database.
        /// </summary>
        /// <param name="logic">DevelopmentLogic type parameter.</param>
        public static void AveragePrice(DevelopmentLogic logic)
        {
            if (logic != null)
            {
                Console.WriteLine(Math.Round(logic.AverageGamePrice(), 2) + " Ft");
            }

            Console.ReadLine();
        }

        /// <summary>
        /// Static method that lists the name of the games by their developers.
        /// </summary>
        /// <param name="logic">DevelopmentLogic type parameter.</param>
        public static void GamesByCompanies(DevelopmentLogic logic)
        {
            if (logic != null)
            {
                foreach (var item in logic.GamesByCompanies())
                {
                    Console.WriteLine($"[{item.CompanyName}] : {item.GameTitles}\n");
                }
            }

            Console.ReadLine();
        }

        /// <summary>
        /// A sub-menu within the main menu for Game specific methods.
        /// </summary>
        /// <param name="logic">DevelopmentLogic type parameter.</param>
        public static void GamesMenu(DevelopmentLogic logic)
        {
                var gmenu = new ConsoleMenu().Add("-- GET ONE GAME --", () =>
                                             {
                                                 Console.WriteLine("Give me an id");
                                                 int id = int.Parse(Console.ReadLine());
                                                 Data.Game g = logic.GetOneGame(id);
                                                 if (g != null)
                                                 {
                                                    Console.WriteLine(g);
                                                 }
                                                 else
                                                 {
                                                     Console.WriteLine("No Game exists with this ID.");
                                                 }

                                                 Console.ReadLine();
                                             })
                                             .Add("-- GET ALL GAMES --", () =>
                                             {
                                                 foreach (var item in logic.GetAllGames())
                                                 {
                                                     Console.WriteLine(item);
                                                 }

                                                 Console.ReadLine();
                                             })
                                             .Add("-- INSERT GAME --", () =>
                                             {
                                                 try
                                                 {
                                                     Console.WriteLine("What's the game called?");
                                                     string name = Console.ReadLine();
                                                     Console.WriteLine("When did/will it release? FORMAT : YYYY/MM/DD");
                                                     string datestring = Console.ReadLine();
                                                     DateTime date = new DateTime(int.Parse(datestring.Split('/')[0]), int.Parse(datestring.Split('/')[1]), int.Parse(datestring.Split('/')[2]));
                                                     Console.WriteLine("What does/will it cost?");
                                                     int price = int.Parse(Console.ReadLine());
                                                     Console.WriteLine("What genre does it belong in?");
                                                     string genre = Console.ReadLine();
                                                     Console.WriteLine("What type is it? (FPS/TPS)");
                                                     string type = Console.ReadLine();
                                                     Console.WriteLine("What's the id of the company who made it?");
                                                     int cid = int.Parse(Console.ReadLine());
                                                     Console.WriteLine("What's the id of the platform you want it to be released on?");
                                                     int pid = int.Parse(Console.ReadLine());

                                                     logic.CreateGame(name, date, price, genre, type, cid, pid);
                                                     Console.WriteLine("Insert was succesful!");
                                                     Console.ReadLine();
                                                 }
                                                 catch (FormatException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("Please use a correct Format.");
                                                     Console.ReadLine();
                                                 }
                                                 catch (IndexOutOfRangeException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("Please use a correct Format.");
                                                     Console.ReadLine();
                                                 }
                                                 catch (OverflowException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("Please use a reasonable number.");
                                                     Console.ReadLine();
                                                 }
                                                 catch (ArgumentOutOfRangeException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("This can't be set as a correct date.");
                                                     Console.ReadLine();
                                                 }
                                             })
                                             .Add("-- CHANGE GAME'S PRICE --", () =>
                                             {
                                                 Console.WriteLine("What's the id of the game whose price you want to change?");
                                                 int id = int.Parse(Console.ReadLine());
                                                 Console.WriteLine("What do you want the new price to be?");
                                                 int newprice = int.Parse(Console.ReadLine());
                                                 try
                                                 {
                                                     logic.ChangeGamePrice(id, newprice);
                                                     Console.WriteLine("\nCHANGE SUCCESFUL!");
                                                 }
                                                 catch (InvalidCastException e)
                                                 {
                                                     Console.WriteLine(e.Message);
                                                 }

                                                 Console.ReadLine();
                                             })
                                             .Add("-- REMOVE GAME --", () =>
                                             {
                                                 Console.WriteLine("Give me the id of the game which you want to delete.");
                                                 int id = int.Parse(Console.ReadLine());
                                                 Console.WriteLine("\nOUTCOME:");
                                                 Console.WriteLine(logic.RemoveGame(logic.GetOneGame(id)));
                                                 Console.ReadLine();
                                             })
                                             .Add("-- BACK --", ConsoleMenu.Close);

                gmenu.Show();
        }

        /// <summary>
        /// A sub-menu within the main menu for Company specific methods.
        /// </summary>
        /// <param name="logic">DevelopmentLogic type parameter.</param>
        public static void CompanyMenu(DevelopmentLogic logic)
        {
                var cmenu = new ConsoleMenu().Add("-- GET ONE COMPANY --", () =>
                                             {
                                                 Console.WriteLine("Give me an id");
                                                 int id = int.Parse(Console.ReadLine());
                                                 Data.Company c = logic.GetOneCompany(id);
                                                 if (c != null)
                                                 {
                                                     Console.WriteLine(c);
                                                 }
                                                 else
                                                 {
                                                     Console.WriteLine("No Company exists with this ID.");
                                                 }

                                                 Console.ReadLine();
                                             })
                                             .Add("-- GET ALL COMPANIES --", () =>
                                             {
                                                 foreach (var item in logic.GetAllCompanies())
                                                 {
                                                     Console.WriteLine(item);
                                                 }

                                                 Console.ReadLine();
                                             })
                                             .Add("-- INSERT COMPANY --", () =>
                                             {
                                                 try
                                                 {
                                                     Console.WriteLine("What's the company called?");
                                                     string name = Console.ReadLine();
                                                     Console.WriteLine("When was it founded? FORMAT : YYYY/MM/DD");
                                                     string datestring = Console.ReadLine();
                                                     DateTime date = new DateTime(int.Parse(datestring.Split('/')[0]), int.Parse(datestring.Split('/')[1]), int.Parse(datestring.Split('/')[2]));
                                                     Console.WriteLine("In what city it's headquarter is?");
                                                     string city = Console.ReadLine();
                                                     Console.WriteLine("In what country it's headquarter is?");
                                                     string country = Console.ReadLine();
                                                     Console.WriteLine("Who is the boss of this company?");
                                                     string boss = Console.ReadLine();

                                                     logic.CreateCompany(name, date, city, country, boss);
                                                     Console.WriteLine("Insert was succesful!");
                                                     Console.ReadLine();
                                                 }
                                                 catch (FormatException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("Please use a correct Format.");
                                                     Console.ReadLine();
                                                 }
                                                 catch (IndexOutOfRangeException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("Please use a correct Format.");
                                                     Console.ReadLine();
                                                 }
                                                 catch (ArgumentOutOfRangeException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("This can't be set as a correct date.");
                                                     Console.ReadLine();
                                                 }
                                             })
                                             .Add("-- CHANGE COMPANY'S CEO --", () =>
                                             {
                                                 Console.WriteLine("What's the id of the company whose boss you want to change?");
                                                 int id = int.Parse(Console.ReadLine());
                                                 Console.WriteLine("Who do you want the new boss to be?");
                                                 string newboss = Console.ReadLine();
                                                 try
                                                 {
                                                     logic.ChangeCompanyBoss(id, newboss);
                                                     Console.WriteLine("\nCHANGE SUCCESFUL!");
                                                 }
                                                 catch (InvalidCastException e)
                                                 {
                                                     Console.WriteLine(e.Message);
                                                 }

                                                 Console.ReadLine();
                                             })
                                             .Add("-- REMOVE COMPANY --", () =>
                                             {
                                                 Console.WriteLine("Give me the id of the company which you want to delete.");
                                                 int id = int.Parse(Console.ReadLine());
                                                 Console.WriteLine("\nOUTCOME:");
                                                 Console.WriteLine(logic.RemoveCompany(logic.GetOneCompany(id)));
                                                 Console.ReadLine();
                                             })
                                             .Add("-- BACK --", ConsoleMenu.Close);

                cmenu.Show();
        }

        /// <summary>
        /// A sub-menu within the main menu for Platform specific methods.
        /// </summary>
        /// <param name="logic">DeviceProduceLogic type parameter.</param>
        public static void PlatformMenu(DeviceProduceLogic logic)
        {
                var pmenu = new ConsoleMenu().Add("-- GET ONE PLATFORM --", () =>
                                             {
                                                 Console.WriteLine("Give me an id");
                                                 int id = int.Parse(Console.ReadLine());
                                                 Data.Platform p = logic.GetOnePlatform(id);
                                                 if (p != null)
                                                 {
                                                     Console.WriteLine(p);
                                                 }
                                                 else
                                                 {
                                                     Console.WriteLine("No Platform exists with this ID.");
                                                 }

                                                 Console.ReadLine();
                                             })
                                             .Add("-- GET ALL PLATFORMS --", () =>
                                             {
                                                 foreach (var item in logic.GetAllPlatforms())
                                                 {
                                                     Console.WriteLine(item);
                                                 }

                                                 Console.ReadLine();
                                             })
                                             .Add("-- INSERT PLATFORM --", () =>
                                             {
                                                 try
                                                 {
                                                     Console.WriteLine("What's the platform called?");
                                                     string name = Console.ReadLine();
                                                     Console.WriteLine("When did/will it release? FORMAT : YYYY/MM/DD");
                                                     string datestring = Console.ReadLine();
                                                     DateTime date = new DateTime(int.Parse(datestring.Split('/')[0]), int.Parse(datestring.Split('/')[1]), int.Parse(datestring.Split('/')[2]));
                                                     Console.WriteLine("What does/will it cost?");
                                                     int price = int.Parse(Console.ReadLine());
                                                     Console.WriteLine("How big is it's storage capacity?");
                                                     int storage = int.Parse(Console.ReadLine());
                                                     Console.WriteLine("Does it have an hdmi port? (y/n)");
                                                     bool hdmi = false;
                                                     bool t = false;
                                                     int index = 0;
                                                     while (!t)
                                                     {
                                                         if (index++ > 0)
                                                         {
                                                             Console.WriteLine("You can only type y or n\ty - YES n - NO");
                                                         }

                                                         string answer = Console.ReadLine();
                                                         if (answer.ToLower() == "y")
                                                         {
                                                             hdmi = true;
                                                             t = true;
                                                         }
                                                         else if (answer.ToLower() == "n")
                                                         {
                                                             t = true;
                                                         }
                                                     }

                                                     Console.WriteLine("What's the id of the company who produced this platform?");
                                                     int cid = int.Parse(Console.ReadLine());

                                                     logic.CreatePlatform(name, date, price, storage, hdmi, cid);
                                                     Console.WriteLine("Insert was succesful!");
                                                     Console.ReadLine();
                                                 }
                                                 catch (FormatException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("Please use a correct Format.");
                                                     Console.ReadLine();
                                                 }
                                                 catch (IndexOutOfRangeException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("Please use a correct Format.");
                                                     Console.ReadLine();
                                                 }
                                                 catch (OverflowException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("Please use a reasonable number.");
                                                     Console.ReadLine();
                                                 }
                                                 catch (ArgumentOutOfRangeException)
                                                 {
                                                     Console.WriteLine("INSERT FAILED");
                                                     Console.WriteLine("This can't be set as a correct date.");
                                                     Console.ReadLine();
                                                 }
                                             })
                                             .Add("-- CHANGE PLATFORM'S STORAGE --", () =>
                                             {
                                                 Console.WriteLine("What's the id of the platform which storage you want to change?");
                                                 int id = int.Parse(Console.ReadLine());
                                                 Console.WriteLine("What do you want the new storage capacity to be?");
                                                 int newcapacity = int.Parse(Console.ReadLine());
                                                 try
                                                 {
                                                     logic.ChangePlatformStorage(id, newcapacity);
                                                     Console.WriteLine("\nCHANGE SUCCESFUL!");
                                                 }
                                                 catch (InvalidCastException e)
                                                 {
                                                     Console.WriteLine(e.Message);
                                                 }

                                                 Console.ReadLine();
                                             })
                                             .Add("-- REMOVE PLATFORM --", () =>
                                             {
                                                 Console.WriteLine("Give me the id of the platform which you want to delete.");
                                                 int id = int.Parse(Console.ReadLine());
                                                 Console.WriteLine("\nOUTCOME:");
                                                 Console.WriteLine(logic.RemovePlatform(logic.GetOnePlatform(id)));
                                                 Console.ReadLine();
                                             })
                                             .Add("-- BACK --", ConsoleMenu.Close);

                pmenu.Show();
        }
    }
}
