﻿// <copyright file="GlobalSuppressions.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "It had to be protected type because other classes inherit it.", Scope = "member", Target = "~F:GameDataBaseFF.Repository.ParentRepo`1.ctx")]
[assembly: SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "It's only visible for the class's descendants.", Scope = "member", Target = "~F:GameDataBaseFF.Repository.ParentRepo`1.ctx")]
[assembly: SuppressMessage("Design", "CA1012:Abstract types should not have public constructors", Justification = "Have to have a public constructor.", Scope = "type", Target = "~T:GameDataBaseFF.Repository.ParentRepo`1")]
[assembly: SuppressMessage("", "CA1014", Justification = "<NikGitStats>", Scope = "module")]