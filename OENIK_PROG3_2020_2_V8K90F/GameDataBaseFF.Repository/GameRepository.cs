﻿// <copyright file="GameRepository.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GameDataBaseFF.Data;
    using GameDataBaseFF.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Game type Repository class. Descends from Repository as Game, and implements IGameRepository.
    /// </summary>
    public class GameRepository : ParentRepo<Game>, IGameRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GameRepository"/> class.
        /// </summary>
        /// <param name="ctx">It will be given to the parent constructor.</param>
        public GameRepository(DbContext ctx)
                                            : base(ctx)
        {
        }

        /// <summary>
        /// Methot that is used to change the price of a game.
        /// </summary>
        /// <param name="id">The id of the platform.</param>
        /// <param name="newprice">The new storage capacity.</param>
        public void ChangePrice(int id, int newprice)
        {
            var game = this.GetOne(id);
            if (game != null)
            {
                game.Price = newprice;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException("This object does not exist.");
            }
        }

        /// <summary>
        /// Method that is used to get a Game entity from the Game type set.
        /// </summary>
        /// <param name="id">The id of the returnable entity.</param>
        /// <returns>Returns a Game entity.</returns>
        public override Game GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id.Equals(id));
        }

        /// <summary>
        /// Method that is used to insert a game entity into the game type set.
        /// </summary>
        /// <param name="entity">The entity that we want to insert into the set.</param>
        public override void Insert(Game entity)
        {
            (this.ctx as GamesContext).Games.Add(entity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Method that is used to Remove a Game entity from the game set.
        /// </summary>
        /// <param name="entity">The entity that we want to remove from the set.</param>
        /// <returns>Whether the removal was successful or not.</returns>
        public override bool Remove(Game entity)
        {
            if (entity != null && this.GetOne(entity.Id) != null)
            {
                (this.ctx as GamesContext).Games.Remove(entity);
                this.ctx.SaveChanges();
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public bool ChangeGame(int id, string name, DateTime release, string genre, string type, int price, int company, int platform)
        {
            var game = this.GetOne(id);
            if (game == null)
            {
                return false;
            }

            game.Name = name;
            game.CompanyId = company;
            game.PlatformId = platform;
            game.Price = price;
            game.Type = type;
            game.Genre = genre;
            game.ReleaseDate = release;
            this.ctx.SaveChanges();
            return true;
        }
    }
}
