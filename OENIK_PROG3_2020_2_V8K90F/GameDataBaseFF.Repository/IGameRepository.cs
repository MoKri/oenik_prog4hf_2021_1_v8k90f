﻿// <copyright file="IGameRepository.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GameDataBaseFF.Data;

    /// <summary>
    /// Interface that contains an empty method for the UPDATE action from the CRUD actions for the Game entities.
    /// It implements the Irepository interface as Game.
    /// </summary>
    public interface IGameRepository : IRepository<Game>
    {
        /// <summary>
        /// An empty method that will be used to be the UPDATE action for the Game entites.
        /// </summary>
        /// <param name="id">The id of the platform that we will want to update.</param>
        /// <param name="newprice">The parameter that will be used to update the entity.</param>
        void ChangePrice(int id, int newprice);

        /// <summary>
        /// Changes an entire game.
        /// </summary>
        /// <param name="id">the id of the game.</param>
        /// <param name="name">the name of the game.</param>
        /// <param name="release">the release of the game.</param>
        /// <param name="genre">the genre of the game.</param>
        /// <param name="type">the type of the game.</param>
        /// <param name="price">the price of the game.</param>
        /// <param name="company">the company of the game.</param>
        /// <param name="platform">the platform of the game.</param>
        /// <returns>Whether the game changed or not.</returns>
        bool ChangeGame(int id, string name, DateTime release, string genre, string type, int price, int company, int platform);
    }
}
