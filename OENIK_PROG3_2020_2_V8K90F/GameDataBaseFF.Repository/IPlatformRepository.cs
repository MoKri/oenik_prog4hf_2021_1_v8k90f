﻿// <copyright file="IPlatformRepository.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GameDataBaseFF.Data;

    /// <summary>
    /// Interface that contains an empty method for the UPDATE action from the CRUD actions for the Platform entities.
    /// </summary>
    public interface IPlatformRepository : IRepository<Platform>
    {
        /// <summary>
        /// An empty method that will be used to be the UPDATE action for the Platform entites.
        /// </summary>
        /// <param name="id">The id of the platform that we want to update.</param>
        /// <param name="newStorage">The parameter that will be used to update the entity.</param>
        void ChangeStorage(int id, int newStorage);
    }
}
