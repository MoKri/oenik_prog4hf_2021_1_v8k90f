﻿// <copyright file="ICompanyRepository.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using GameDataBaseFF.Data;

    /// <summary>
    /// Interface that contains an empty method for the UPDATE action from the CRUD actions for the Company entities.
    /// It implements the Irepository interface as Company.
    /// </summary>
    public interface ICompanyRepository : IRepository<Company>
    {
        /// <summary>
        /// An empty method that will be used to be the UPDATE action for the Company entites.
        /// </summary>
        /// <param name="id">The id of the platform that we will want to update.</param>
        /// <param name="newBoss">The parameter that will be used to update the entity.</param>
        void ChangeBoss(int id, string newBoss);
    }
}
