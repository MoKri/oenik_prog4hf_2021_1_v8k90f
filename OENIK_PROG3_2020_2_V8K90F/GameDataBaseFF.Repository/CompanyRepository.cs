﻿// <copyright file="CompanyRepository.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GameDataBaseFF.Data;
    using GameDataBaseFF.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Company type Repository class. Descends from Repository as Company, and implements ICompanyRepository.
    /// </summary>
    public class CompanyRepository : ParentRepo<Company>, ICompanyRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CompanyRepository"/> class.
        /// </summary>
        /// <param name="ctx">It will be given to the parent constructor.</param>
        public CompanyRepository(DbContext ctx)
                                                : base(ctx)
        {
        }

        /// <summary>
        ///  Methot that is used to change the CEO of a company.
        /// </summary>
        /// <param name="id">The id of the Company.</param>
        /// <param name="newBoss">The name of the new CEO.</param>
        public void ChangeBoss(int id, string newBoss)
        {
            var company = this.GetOne(id);
            if (company != null)
            {
                company.CEO = newBoss;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException("This object does not exist.");
            }
        }

        /// <summary>
        /// Method that is used to get a Company entity from the Company type set.
        /// </summary>
        /// <param name="id">The id of the returnable entity.</param>
        /// <returns>Returns a Company entity.</returns>
        public override Company GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id.Equals(id));
        }

        /// <summary>
        ///  Method that is used to insert a game entity into the game type set.
        /// </summary>
        /// <param name="entity">The entity that we want to insert into the Company set.</param>
        public override void Insert(Company entity)
        {
            (this.ctx as GamesContext).Companies.Add(entity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Method that is used to Remove a Company entity from the Company set.
        /// </summary>
        /// <param name="entity">The removable entity.</param>
        /// <returns>Whether the removal was succesful or not.</returns>
        public override bool Remove(Company entity)
        {
            if (entity != null && this.GetOne(entity.Id) != null)
            {
                (this.ctx as GamesContext).Companies.Remove(entity);
                this.ctx.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
