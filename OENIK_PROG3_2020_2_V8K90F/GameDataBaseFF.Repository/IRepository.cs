﻿// <copyright file="IRepository.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;

    /// <summary>
    /// Generic interface that will be used on Repositories.
    /// </summary>
    /// <typeparam name="T">The generic parameter.</typeparam>
    public interface IRepository<T>
                                    where T : class
    {
        /// <summary>
        /// Empty method that will be implemented later, somewhere else.
        /// This method will be used to get a T type entity from a T type set.
        /// </summary>
        /// <param name="id">The id of the returnable entity.</param>
        /// <returns>A T type entity.</returns>
        T GetOne(int id);

        /// <summary>
        /// Empty method that will be implemented later, somewhere else.
        /// This method will be used to get a T type set from the database.
        /// </summary>
        /// <returns>A T type set.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Empty method that will be implemented later, somewhere else.
        /// This method will be used to Insert a T type entity into a T type set.
        /// </summary>
        /// <param name="entity">The insertable entity.</param>
        void Insert(T entity);

        /// <summary>
        /// Empty method that will be implemented later, somewhere else.
        /// This method will be used to remove a T type entity from a T type set.
        /// </summary>
        /// <param name="entity">The removable entity.</param>
        /// <returns>Whether the removal was succesful or not.</returns>
        bool Remove(T entity);
    }
}
