﻿// <copyright file="ParentRepo{T}.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GameDataBaseFF.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Abstract, generic Repository class that implements the IRepository interface.
    /// Database entity type repositories are descended from here.
    /// </summary>
    /// <typeparam name="T">The generic parameter.</typeparam>
    public abstract class ParentRepo<T> : IRepository<T>
                                                    where T : class
    {
        /// <summary>
        /// A DbContext instance, which is used for the database.
        /// </summary>
        protected DbContext ctx;

        /// <summary>
        /// Initializes a new instance of the <see cref="ParentRepo{T}"/> class.
        /// Gets a context, and sets it as it's own.
        /// </summary>
        /// <param name="ctx">The context it gets.</param>
        public ParentRepo(DbContext ctx)
        {
            this.ctx = ctx;
        }

        /// <summary>
        /// Method, that's used to get every data from a set.
        /// </summary>
        /// <returns>T type data set.</returns>
        public IQueryable<T> GetAll()
        {
            return this.ctx.Set<T>();
        }

        /// <summary>
        /// Abstract method, that will return an element from a set.
        /// </summary>
        /// <param name="id">Id of the returnable element.</param>
        /// <returns>An element from a T type set.</returns>
        public abstract T GetOne(int id);

        /// <summary>
        /// Abstract method that will be used to insert a T type element into the T type set.
        /// </summary>
        /// <param name="entity">The element that will be inserted.</param>
        public abstract void Insert(T entity);

        /// <summary>
        /// Abstact method that will be used to remove a T type element from a T type set.
        /// </summary>
        /// <param name="entity">The element that will be removed.</param>
        /// <returns>Whether the entity was deleted succesfully or not.</returns>
        public abstract bool Remove(T entity);
    }
}
