﻿// <copyright file="PlatformRepository.cs" company="Molnár Krisztofer Szebasztián - V8K90F">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace GameDataBaseFF.Repository
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using GameDataBaseFF.Data;
    using GameDataBaseFF.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Platform type Repository class. Descends from Repository as Platform, and implements IPlatformRepository.
    /// </summary>
    public class PlatformRepository : ParentRepo<Platform>, IPlatformRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="PlatformRepository"/> class.
        /// </summary>
        /// <param name="ctx">It will be given to the parent constructor.</param>
        public PlatformRepository(DbContext ctx)
                                                    : base(ctx)
        {
        }

        /// <summary>
        /// Methot that is used to change the storage capacity of a platform.
        /// </summary>
        /// <param name="id">The id of the platform.</param>
        /// <param name="newStorage">The new storage capacity.</param>
        public void ChangeStorage(int id, int newStorage)
        {
            var platform = this.GetOne(id);
            if (platform != null)
            {
                platform.Storage = newStorage;
                this.ctx.SaveChanges();
            }
            else
            {
                throw new InvalidOperationException("This object does not exist.");
            }
        }

        /// <summary>
        /// Method that is used to get a Platform wntity from the Platform type set.
        /// </summary>
        /// <param name="id">The id of the returnable entity.</param>
        /// <returns>A platform type entity.</returns>
        public override Platform GetOne(int id)
        {
            return this.GetAll().SingleOrDefault(x => x.Id.Equals(id));
        }

        /// <summary>
        /// Method that is used to insert a Platform entity into the platform type set.
        /// </summary>
        /// <param name="entity">The entity that we want to insert.</param>
        public override void Insert(Platform entity)
        {
            (this.ctx as GamesContext).Platforms.Add(entity);
            this.ctx.SaveChanges();
        }

        /// <summary>
        /// Method that is used to remove a platform entity from the platform type set.
        /// </summary>
        /// <param name="entity">The removable entity.</param>
        /// <returns>Whether the removal was successful or not.</returns>
        public override bool Remove(Platform entity)
        {
            if (entity != null && this.GetOne(entity.Id) != null)
            {
                (this.ctx as GamesContext).Platforms.Remove(entity);
                this.ctx.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
